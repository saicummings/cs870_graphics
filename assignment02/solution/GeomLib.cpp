#include "GeomLib.h"

//
// Copy all components of "other" into this point.
//
void Float4::copyFrom(const Float4& other) {
    for (int i = 0; i < 4; i++)
        v[i] = other.v[i];
}

//
// Add all four components of this and "other" into sum.
//
void Float4::plus(const Float4& other, Float4& sum) const {
    for (int i = 0; i < 4; i++)
        sum.v[i] = v[i] + other.v[i];
}

//
// Subtract all four components of "other" from this point, into difference
//
void Float4::minus(const Float4& other, Float4& difference) const {
    for (int i = 0; i < 4; i++)
        difference.v[i] = v[i] - other.v[i];
}

//
// Dot product of all four (X Y Z W) components of "other" and this point.
//
float Float4::dot(const Float4& other) const {
    double sum = 0;
    for (int i = 0; i < 4; i++)
        sum += v[i] * other.v[i];
    return sum;
}

//
// Divide all 4 of this point components by W (if W==0, do nothing),
// into result.
//
void Float4::homogenize(Float4& result) const {
    double w = v[3];
    if (w != 0) {
        for (int i = 0; i < 4; i++)
            result[i] /= w;
    }
}

//
// Multiply all four (X Y Z W) components of this point by scale factor,
// into result.
//
void Float4::times(float factor, Float4& result) const {
    for (int i = 0; i < 4; i++)
        result.v[i] = factor * v[i];
}


//
// return the distance from this point to "other".
//
float Point4::distanceTo(Point4& other) const {
    Vector4 offset = *this - other;
    return offset.length();
}

//
// Length, using three (X Y Z) components of this point.
//
float Vector4::length() const {
    return sqrt(*this * *this);
}

//
// Divide first three (X Y Z) components of this point by length,
// into result (if length==0, do nothing)
//
void Vector4::normalize(Vector4& result) const {
    double l = this->length();
    if (l != 0) {
        times(1/l, result);
    }
}

//
// Cross product of this vector x "other", into result
// Use only X Y Z components.
// Sets W component of product to 0.
//
void Vector4::cross(const Vector4& other, Vector4& result) const {
    result.v[0] = v[1] * other.v[2] - v[2] * other.v[1];
    result.v[1] = v[2] * other.v[0] - v[0] * other.v[2];
    result.v[2] = v[0] * other.v[1] - v[1] * other.v[0];
}

//
// return the angle (in radians) between this vector and "other".
// If this or other is (0 0 0), return 0 angle.
//
float Vector4::angle(Vector4& other) const {
    double cosine = (*this * other) / (length() * other.length());
    return acos(cosine);
}

//
// Copy all components of other matrix into this.
//
void Matrix4::copyFrom(const Matrix4& other) {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            m[row][col] = other.m[row][col];
        }
    }
}

//
// Add all components of this and "other" matrix into sum
//
void Matrix4::plus(const Matrix4& other, Matrix4& sum) const {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            sum.m[row][col] = m[row][col] + other.m[row][col];
        }
    }
}

//
// Subtract all components of this matrix minus "other", into difference
//
void Matrix4::minus(const Matrix4& other, Matrix4& difference) const {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            difference.m[row][col] = m[row][col] - other.m[row][col];
        }
    }
}

//
// Multiply all components by factor, into product
//
void Matrix4::times(float factor, Matrix4& product) const {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            product.m[row][col] = m[row][col] * factor;
        }
    }
}

//
// Multiply (*this) x ("other"), and put product into product
//
// CAREFULL!! this may == &product!  So watch for partially-modified entries.
// Store result in temp array first!
//
void Matrix4::times(const Matrix4& other, Matrix4 &product) const {
    Matrix4 result;
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            double sum = 0;
            for (int j = 0; j < 4; j++)
                sum += m[row][j] * other.m[j][col];
            result.m[row][col] = sum;
        }
    }

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            product.m[row][col] = result.m[row][col];
        }
    }
}

//
// Multiply (*this) x ("point"), and put resulting point into "product".
// CAREFULL!! point may == product, so watch for partially-modified entries.
// Store result in temp array first!
//
void Matrix4::times(const Float4& point, Float4& product) const {
    Float4 result;
    for (int row = 0; row < 4; row++) {
        double sum = 0;
        for (int j = 0; j < 4; j++)
            sum += m[row][j] * point[j];
        result[row] = sum;
    }

    for (int row = 0; row < 4; row++) {
        product[row] = result[row];
    }
}

//
// Transpose this matrix, put result into result.
//
// CAREFULL!! this may = &result!  So watch for partially-modified entries.
// Store result in temp array first!
//
void Matrix4::transpose(Matrix4& result) const {
    Matrix4 temp;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            temp.m[row][col] = m[col][row];
        }
    }

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            result.m[row][col] = temp.m[row][col];
        }
    }

}

//
// Set this matrix to identity matrix.
//
void Matrix4::setToIdentity() {
    set(1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about X axis.
// "angle" is in DEGREES.
//
void Matrix4::setToXRotation(float angle) {
    double rads = angle * 3.1416 / 180;
    double s = sin(rads);
    double c = cos(rads);
    set(1,0,0,0,
        0,c,-s,0,
        0,s,c,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about Y axis.
// "angle" is in DEGREES.
//
void Matrix4::setToYRotation(float angle) {
    double rads = angle * 3.1416 / 180;
    double s = sin(rads);
    double c = cos(rads);
    set(c,0,s,0,
        0,1,0,0,
        -s,0,c,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about Z axis.
// "angle" is in DEGREES.
//
void Matrix4::setToZRotation(float angle) {
    double rads = angle * 3.1416 / 180;
    double s = sin(rads);
    double c = cos(rads);
    set(c,-s,0,0,
        s,c,0,0,
        0,0,1,0,
        0,0,0,1);
}

//
// Set this matrix to translation matrix.
//
void Matrix4::setToTranslation(float tx, float ty, float tz) {
    set(0,0,0,tx,
        0,0,0,ty,
        0,0,0,tz,
        0,0,0,1);
}

//
// Set this matrix to scaling matrix.
//
void Matrix4::setToScaling(float sx, float sy, float sz) {
    set(sx,0,0,0,
        0,sy,0,0,
        0,0,sz,0,
        0,0,0,1);
}
