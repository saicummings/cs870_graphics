//========================================================================
// Untangle a graph.
// Author: Alejo Hausner
// Date: Sept. 3, 2017.
//
// Heavily modified from an example by Camilla Berglund <elmindreda@glfw.org>
//========================================================================

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "Camera.h"
#include "GeomLib.h"

using namespace std;

Camera cam;

vector<double> vertex_x;
vector<double> vertex_y;
vector< vector<bool> > is_edge;
int n_vertices = 0;
vector<Point4> intersections;

bool mouse_dragging = false;
int cursor_x    = 0;
int cursor_y    = 0;

double vertex_size = 0.1;
int active_vertex = -1;

/////////////////////////////////////////////////////////////////////
// If line segment ab intersects segment cd, returns true,
// and sets p to the intersection point.
// Otherwise returns false, and p is unchanged.
/////////////////////////////////////////////////////////////////////

bool ray_hits_segment(const Point4& a, const Point4& b,
                      const Point4& c, const Point4& d,
                      Point4& p)
{
    Vector4 v = b - a;
    Vector4 cd = d - c;
    Vector4 N(cd.Y(), -cd.X(), 0);
    double num = N * (c - a);
    double denom = N * v;
    if (fabs(denom) < 0.0001)
        return false;
    else {
        double t = num / denom;
        if (t < 0.0001 || t > 0.9999)
            return false;

        Vector4 tv = t * v;
        p = a + tv;
        return true;
    }
}

bool intersects(const Point4& a, const Point4& b,
                const Point4& c, const Point4& d,
                Point4& p)
{
    return
        ray_hits_segment(a,b, c,d, p) &&
        ray_hits_segment(c,d, a,b, p);
}




//////////////////////////////////////////////////////////////////////
// Returns true (and updates the intersection list) if one or
// more edges intersect.  Otherwise returns false.
//////////////////////////////////////////////////////////////////////

bool some_edges_cross()
{
    intersections.clear();
    for (int i1 = 0; i1 < n_vertices; i1++)
        for (int j1 = 0; j1 < n_vertices; j1++)
            if (is_edge[i1][j1]) {
                for (int i2 = i1+1; i2 < n_vertices; i2++)
                    for (int j2 = j1+1; j2 < n_vertices; j2++)
                    {
                        if (is_edge[i2][j2]) {
                            Point4 a(vertex_x[i1], vertex_y[i1], 0);
                            Point4 b(vertex_x[j1], vertex_y[j1], 0);
                            Point4 c(vertex_x[i2], vertex_y[i2], 0);
                            Point4 d(vertex_x[j2], vertex_y[j2], 0);
                            Point4 p;
                            if (intersects(a,b, c,d, p)) {
                                intersections.push_back(p);
                            }
                        }
                    }
            }

    return intersections.size() > 0;
}


//////////////////////////////////////////////////////////////////////
//
// Draw the coordinate grid.
//
// *** NO NEED TO CHANGE THIS ***
//
//////////////////////////////////////////////////////////////////////

void drawCoordGrid()
{
    double x,y;

    glColor3d(.5,.5,.7);    // light blue
    glLineWidth(0.1);         // lines as thin as possible
    glBegin(GL_LINES);      // every two glVertex calls will draw a line segment

    // the minor vertical grid lines
    for (x=-3.0; x<=3.0; x+=0.1)
    {
        glVertex2d(x,-3.0);
        glVertex2d(x, 3.0);
    }
    // the minor horizontal grid lines
    for (y=-3.0; y<=3.0; y+=0.1)
    {
        glVertex2d(-3.0,y);
        glVertex2d( 3.0,y);
    }

    // the major vertical grid lines: lighter blue
    glColor3d(.7,.7,.9);
    for (x=-3.0; x<=3.0; x+=1)
    {
        glVertex2d(x,-3.0);
        glVertex2d(x, 3.0);
    }

    // the major horizontal grid lines
    for (y=-3.0; y<=3.0; y+=1)
    {
        glVertex2d(-3.0,y);
        glVertex2d( 3.0,y);
    }

    // the coordinate axes.
    glColor3d(.9,.7,.7);
    glVertex2d(0.0,-3.0);
    glVertex2d(0.0, 3.0);
    glVertex2d(-3.0,0.0);
    glVertex2d( 3.0,0.0);

    glEnd();
}

//////////////////////////////////////////////////////////////////////
//
// Convenience function to check if the mouse is on a vertex.
//
// Inputs:
//    x,y   - mouse coordinates (they are passed in to handleMouseAction
//            or handleMouseMotion
//    hx,hy - coordinates of the vertex.
//
// Returns:
//    true - the mouse x,is near a vertex.
//
// NOTE:
//    You will have to call this function repeatedly, to
//    identify which vertex the user clicked on.
//
// *** NO NEED TO CHANGE THIS ***
//
//////////////////////////////////////////////////////////////////////

bool on_vertex(int x, int y, double vx, double vy)
{
    cout << "on_vertex (x y)=(" << x << " " << y << ")\n";

    double wx,wy;
    cam.mouse_to_world(x,y, wx,wy);

    cout << "world coords: (x y)=(" << wx << " " << wy << ")\n";
    cout << "vertex coords:(x y)=(" << vx << " " << vy << ")\n";

    double s = vertex_size;

    return vx-s <= wx && wx <= vx+s && vy-s <= wy && wy <= vy+s;
}

int vertex_at(int x, int y) {
    cout << "\nvertex at (" << x << " " << y << ")\n";

    for (int i = 0; i < n_vertices; i++) {
        if (on_vertex(x, y, vertex_x[i], vertex_y[i])) {

            cout << "mouse is on vertex " << i << "!\n";

            return i;
        }
    }

    cout << "mouse isn't on any vertex\n";

    return -1;
}

//////////////////////////////////////////////////////////////////////
//
// ************************ YOU SHOULD CHANGE THIS *******************
// **** ADD OTHER FUNCTIONS IF NECCESSARY   **************************
//
// This function is called when the user presses or releases a
// mouse button.
// Parameters:
//
//    button: possible values: GLUT_LEFT, GLUT_MIDDLE or GLUT_RIGHT
//            Identifies the button.
//
//    state:  possible values: GLUT_UP or GLUT_DOWN
//            The state of the button.
//
//    x,y:    The position of the mouse.
//            x goes from 0 on the left to winWidth-1 on the right.
//            y goes from 0 on the top to winHeight-1 on the bottom.
//
//////////////////////////////////////////////////////////////////////

void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods )
{
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;

    if (action == GLFW_PRESS)
    {
        active_vertex = vertex_at(cursor_x, cursor_y);
        if (active_vertex != -1) {
            mouse_dragging = true;
        }
        else {
            mouse_dragging = false;
        }
    }
    else
    {
        mouse_dragging = false;
        active_vertex = -1;
    }
}

void mouse_position_callback( GLFWwindow* window, double x, double y )
{
    cursor_x = (int)x;
    cursor_y = (int)y;

    if ( mouse_dragging ) {
        double wx, wy;
        cam.mouse_to_world(cursor_x, cursor_y, wx, wy);
        vertex_x[active_vertex] = wx;
        vertex_y[active_vertex] = wy;
    }
}

//////////////////////////////////////////////////////////////////////
//
// main() calls this function to re-draw the scene.
//
// YOU MUST CHANGE THIS!
//
//////////////////////////////////////////////////////////////////////

void display(void)
{
    //
    // Set the background colour to darkish blue-gray.
    //
    bool edges_cross = some_edges_cross();
    if (edges_cross)
        glClearColor(.4f,.4f, 0.6f,1.f);
    else
        glClearColor(.2f,.8f, 0.2f,1.f);

    //
    // OK, now clear the screen with the background colour
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    //
    // draw the coordinate grid
    //
    drawCoordGrid();

    double s = vertex_size / 2;
    glColor3d(1.0, 1.0, 0.0);
    glBegin(GL_QUADS);
    for (int i = 0; i < n_vertices; i++) {
        glVertex2d(vertex_x[i] - s, vertex_y[i] - s);
        glVertex2d(vertex_x[i] + s, vertex_y[i] - s);
        glVertex2d(vertex_x[i] + s, vertex_y[i] + s);
        glVertex2d(vertex_x[i] - s, vertex_y[i] + s);
    }
    glEnd();

    glColor3d(1.0, 0.0, 1.0);
    glLineWidth(5);  // lines are 5 pixels wide
    glBegin(GL_LINES);
    for (int i = 0; i < n_vertices; i++) {
        for (int j = i+1; j < n_vertices; j++) {
            if (is_edge[i][j]) {
                glVertex2d(vertex_x[i], vertex_y[i]);
                glVertex2d(vertex_x[j], vertex_y[j]);
            }
        }
    }
    glEnd();

    if (edges_cross) {
        glColor3d(1.0, 0.0, 0.0);
        glBegin(GL_QUADS);
        for (auto p : intersections) {
            glVertex2d(p.X() - s, p.Y() - s);
            glVertex2d(p.X() + s, p.Y() - s);
            glVertex2d(p.X() + s, p.Y() + s);
            glVertex2d(p.X() - s, p.Y() + s);
        }
        glEnd();
    }
}

//////////////////////////////////////////////////////////////////////
//
// Read the graph from a file.
// The graph is stored as an adjacency matrix.
// First line : number of vertices:
// Each remaining line: a list of 0s and 1s.
// entry (a b) is 1 if there's an edge from vertex "a" to vertex "b",
// entry (a b) is 0 otherwise.
// (note the matrix is symmetric, since this is an undirected graph)
//
// YOU MUST CHANGE THIS!
//
//////////////////////////////////////////////////////////////////////

void read_graph(char *filename)
{
    cout << "read_graph.  filename=" << filename << "\n";

    ifstream ifs(filename, std::ifstream::in);
    ifs >> n_vertices;
    for (int i = 0; i < n_vertices; i++) {
        is_edge.push_back(vector<bool>());
        for (int j = 0; j < n_vertices; j++) {
            int vertex_flag;
            ifs >> vertex_flag;
            is_edge[i].push_back(vertex_flag == 1);
        }
    }
    ifs.close();

    double d_theta = 2.0 * M_PI / n_vertices;
    for (int i = 0; i < n_vertices; i++) {
        double angle = i * d_theta;
        double x = cos(angle);
        double y = sin(angle);
        vertex_x.push_back(x);
        vertex_y.push_back(y);
    }

    cout << "vertices:\n";
    for (int i = 0; i < n_vertices; i++) {
        cout << i << ": (" << vertex_x[i] << " " << vertex_y[i] << ")\n";
    }
    cout << "Done reading graph.  Got " << n_vertices << " vertices\n";
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q ) &&
        action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

int main(int argc, char *argv[])
{
    GLFWwindow* window;
    int w = 600;
    int h = 600;

    if (argc != 2)
    {
        cerr << "Usage:\n";
        cerr << "  untangle <graph-filename>\n";
        cerr << "PRESS <Control-C>\n";
        char line[100];
        cin >> line;
        exit (EXIT_FAILURE);
    }

    read_graph(argv[1]);

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "Untangle a graph", NULL, NULL);
    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    cam = Camera(-1.5,-1.5, 3, 3, w, h, window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window,   mouse_position_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
