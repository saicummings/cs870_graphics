//========================================================================
// Untangle a graph.
// Author: Alejo Hausner, Sai Lekyang
// Date: 09-17-18
//
//
// Heavily modified from an example by Camilla Berglund <elmindreda@glfw.org>
//========================================================================

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "Camera.h"
#include "GeomLib.h"

using namespace std;

Camera cam;

vector<double> vertex_x;
vector<double> vertex_y;
vector< vector<bool> > is_edge;
vector<Point4> intersects;

int mouse_x, mouse_y;
int v_count = 0;
double vertex_size = 0.1;
int selected_vertex = -1;

//////////////////////////////////////////////////////////////////////
//
// Draw the coordinate grid.
//
// *** NO NEED TO CHANGE THIS ***
//
//////////////////////////////////////////////////////////////////////

void drawCoordGrid()
{
    double x,y;

    glColor3d(.5,.5,.7);    // light blue
    glLineWidth(0.1);       // lines as thin as possible
    glBegin(GL_LINES);      // every two glVertex calls will draw a line segment

    // the minor vertical grid lines
    for (x=-3.0; x<=3.0; x+=0.1)
    {
        glVertex2d(x,-3.0);
        glVertex2d(x, 3.0);
    }
    // the minor horizontal grid lines
    for (y=-3.0; y<=3.0; y+=0.1)
    {
        glVertex2d(-3.0,y);
        glVertex2d( 3.0,y);
    }

    // the major vertical grid lines: lighter blue
    glColor3d(.7,.7,.9);
    for (x=-3.0; x<=3.0; x+=1)
    {
        glVertex2d(x,-3.0);
        glVertex2d(x, 3.0);
    }

    // the major horizontal grid lines
    for (y=-3.0; y<=3.0; y+=1)
    {
        glVertex2d(-3.0,y);
        glVertex2d( 3.0,y);
    }

    // the coordinate axes.
    glColor3d(.9,.7,.7);
    glVertex2d(0.0,-3.0);
    glVertex2d(0.0, 3.0);
    glVertex2d(-3.0,0.0);
    glVertex2d( 3.0,0.0);

    glEnd();
}

//////////////////////////////////////////////////////////////////////
//
// Convenience function to check if the mouse is on a vertex.
//
// Inputs:
//    x,y   - mouse coordinates (they came from mouse_motion_callback)
//    vx,vy - coordinates of the vertex.
//
// Returns:
//    true/false - when the mouse is/isn't near a vertex.
//
// NOTE:
//    vertex_at() calls this function repeatedly, to
//    identify which vertex the user clicked on.
//
// *** NO NEED TO CHANGE THIS ***
//
//////////////////////////////////////////////////////////////////////

bool on_vertex(int x, int y, double vx, double vy)
{
    cout << "on_vertex (x y)=(" << x << " " << y << ")\n";

    double wx,wy;
    cam.mouse_to_world(x,y, wx,wy);

    cout << "world coords: (x y)=(" << wx << " " << wy << ")\n";
    cout << "vertex coords:(x y)=(" << vx << " " << vy << ")\n";

    double s = vertex_size;

    return vx-s <= wx && wx <= vx+s && vy-s <= wy && wy <= vy+s;
}

int vertex_at(int x, int y) {
    cout << "\nvertex at (" << x << " " << y << ")\n";

    for (int i = 0; i < (int)vertex_x.size(); i++) {
        if (on_vertex(x, y, vertex_x[i], vertex_y[i])) {

            cout << "mouse is on vertex " << i << "!\n";

            return i;
        }
    }

    cout << "mouse isn't on any vertex\n";

    return -1;
}

//////////////////////////////////////////////////////////////////////
//
// ************************ YOU SHOULD CHANGE THIS *******************
// **** ADD OTHER FUNCTIONS IF NECCESSARY   **************************
//
// This function is called when the user presses or releases a
// mouse button.
// Parameters:
//
//    button: possible values: GLFW_MOUSE_BUTTON_LEFT, or GLFW_MOUSE_BUTTON_RIGHT
//            Identifies the button.
//
//    action: possible values: GLFW_PRESS or GLFW_RELEASE
//            The state of the button.
//
//    mods  : modifier keys currently being held down.
//            possible values: GLFW_MOD_SHIFT, GLFW_MOD_CONTROL, GLFW_MOD_ALT
//
//////////////////////////////////////////////////////////////////////

void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods )
{
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;

    if (action == GLFW_PRESS)
    {
        // The mouse position was saved by mouse_position_callback()
        // into mouse_x, mouse_y
        double wx, wy;
        cam.mouse_to_world(mouse_x, mouse_y, wx, wy);        

        // Here, find out if mouse was pressed on some vertex,
        // and record the fact that this happened
        selected_vertex = vertex_at(mouse_x, mouse_y);
    }
    else {
        // Here, record the fact that NOTHING is being pressed
        // on anymore.
        selected_vertex = -1;
    }
}

void mouse_position_callback( GLFWwindow* window, double x, double y )
{
    // Here, if mouse is currently pressing on some vertex,
    // change the vertex's position.
    mouse_x = (int)x;
    mouse_y = (int)y;
    if ( selected_vertex != -1 ) {
        double wx, wy;
        cam.mouse_to_world(mouse_x, mouse_y, wx, wy);
        vertex_x[selected_vertex] = wx;
        vertex_y[selected_vertex] = wy;
    }
}

void check_intersects(){
    // loop through all the verticies and check for intersections
    vector< Point4 > p_vector;

    for (int i = 0; i < v_count; i++){
        for (int j = i + 1; j < v_count; j++) {
            if (is_edge[i][j]) {
                p_vector.push_back(Point4(vertex_x[i], vertex_y[i], 0));
                p_vector.push_back(Point4(vertex_x[j], vertex_y[j], 0));
            }
        }
    }

    int p_size =  p_vector.size();

    for (int i = 0; i < p_size; i++){
        Point4 p1 = p_vector[i++];
        Point4 p2 = p_vector[i];
        
        for (int j = i + 1; j < p_size; j++) {
            Point4 p3 = p_vector[j++];
            Point4 p4 = p_vector[j];
            Vector4 v1 = p2 - p1;
            Vector4 v2 = p4 - p3;

            // compare the 2 vectors to see if intersect
            // get n for the first vector
            Vector4 n(-v1.Y(), v1.X(), 0);

            double t = (n * (p1 - p3)) / (( n * v2));
            // check t and check to see if parrallel
            if (t < 0.001 || t > 0.999 || fabs(n * v2) < 0.001){
                cout << endl;
                continue;
            }

            v1 = p4 - p3;
            v2 = p2 - p1;

            // compare the 2 vectors to see if intersect
            // get n for the first vector
            n.set(-v1.Y(), v1.X(), 0);

            t = (n * (p3 - p1)) / (( n * v2));
            // check t and check to see if parrallel
            if (t < 0.001 || t > 0.999 || fabs(n * v2) < 0.001){
                cout << endl;
                continue;
            }

            Vector4 intersect_v = t * v2;
            Point4 intersect_p = p1 + intersect_v;

            intersects.push_back(intersect_p);
        }
    }
}

//////////////////////////////////////////////////////////////////////
//
// main() calls this function to re-draw the scene.
//
// YOU MUST CHANGE THIS!
//
//////////////////////////////////////////////////////////////////////

void display(void)
{
    //
    // Set the background colour to darkish blue-gray.
    // You should change this, if there no intersections.
    //
    
    intersects.clear();
    check_intersects();
    int intz = intersects.size();
    if ( intz > 0 )
        glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    else
        glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

    //
    // OK, now clear the screen with the background colour
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    //
    // draw the coordinate grid
    //
    drawCoordGrid();

    // Draw a little box (a vertex)
    // You should draw ALL the vertices

    // Draw each vertices based on the numbers of the in file

    double s = vertex_size / 2;
    glColor3d(1.0, 1.0, 0.0);
    glBegin(GL_QUADS);
    for (int i = 0; i < v_count; i++) {
        glVertex2d(vertex_x[i] - s, vertex_y[i] - s);
        glVertex2d(vertex_x[i] + s, vertex_y[i] - s);
        glVertex2d(vertex_x[i] + s, vertex_y[i] + s);
        glVertex2d(vertex_x[i] - s, vertex_y[i] + s);
    }
    glEnd();


    // Draw a line segment (an edge)
    // You should draw ALL the edges
    glColor3d(1.0, 0.0, 1.0);
    glLineWidth(5);  // lines are 5 pixels wide
    glBegin(GL_LINES);
    for (int i = 0; i < v_count; i++) {
        // checking only the top diagonal right of the adjaceny matrix 
        for (int j = i + 1; j < v_count; j++) {
            if (is_edge[i][j]) {
                glVertex2d(vertex_x[i], vertex_y[i]);
                glVertex2d(vertex_x[j], vertex_y[j]);
            }
        }
    }
    glEnd();

    // Finally, draw the intersection points, if any.
    glColor3d(1.0, 0.0, 0.0);
    glBegin(GL_QUADS);
    for (int i = 0; i < (int) intersects.size(); i++) {
        glVertex2d(intersects[i].X() - s, intersects[i].Y() - s);
        glVertex2d(intersects[i].X() + s, intersects[i].Y() - s);
        glVertex2d(intersects[i].X() + s, intersects[i].Y() + s);
        glVertex2d(intersects[i].X() - s, intersects[i].Y() + s);
    }
    glEnd();
}

//////////////////////////////////////////////////////////////////////
//
// Read the graph from a file.
// The graph is stored as an adjacency matrix.
// First line : number of vertices:
// Each remaining line: a list of 0s and 1s.
// entry (a b) is 1 if there's an edge from vertex "a" to vertex "b",
// entry (a b) is 0 otherwise.
// (note the matrix is symmetric, since this is an undirected graph)
//
// YOU MUST CHANGE THIS!
//
//////////////////////////////////////////////////////////////////////

void read_graph(char *filename)
{
    ifstream ifs(filename, ifstream::in);

    // get the number of vertices
    ifs >> v_count;

    // check each ifs line of the adjagency matrix
    for (int i = 0; i < v_count; i++) {
        is_edge.push_back(vector<bool>());
        for (int j = 0; j < v_count; j++) {
            int e;
            ifs >> e;
            is_edge[i].push_back(e == 1);
        }
    }
    ifs.close();
    
    // getting the position of each vertex on the "circle"
    double theta = 2 * M_PI / v_count;
    for (int i = 0; i < v_count; i++) {
        double angle = i * theta;
        vertex_x.push_back(cos(angle));
        vertex_y.push_back(sin(angle));
    }
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q ) &&
        action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

int main(int argc, char *argv[])
{
    GLFWwindow* window;
    int w = 600;
    int h = 600;

    if (argc != 2)
    {
        cerr << "Usage:\n";
        cerr << "  untangle <graph-filename>\n";
        cerr << "PRESS <Control-C>\n";
        char line[100];
        cin >> line;
        exit (EXIT_FAILURE);
    }

    read_graph(argv[1]);

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "Untangle a graph", NULL, NULL);
    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    cam = Camera(-1.5,-1.5, 3, 3, w, h, window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window,   mouse_position_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
