#include "GeomLib.h"

//
// Copy all components of "other" into this point.
//
void Float4::copyFrom(const Float4& other) {
    for (int i = 0; i < 4; i++){
        v[i] = other.v[i];
    }
}

//
// Add all four components of this and "other" into sum.
//
void Float4::plus(const Float4& other, Float4& sum) const {
    for (int i = 0; i < 4; i++){
        sum.v[i] = v[i] + other.v[i];
    }
}

//
// Subtract all four components of "other" from this point, into difference
//
void Float4::minus(const Float4& other, Float4& difference) const {
    for (int i = 0; i < 4; i++){
        difference.v[i] = v[i] - other.v[i];
    }
}

//
// Dot product of all four (X Y Z W) components of "other" and this point.
//
float Float4::dot(const Float4& other) const {
    float dot = 0;
    for (int i = 0; i < 4; i++){
        dot += v[i] * other.v[i];
    }
    return dot;
}

//
// Divide all 4 of this point components by W (if W==0, do nothing),
// into result.
//
void Float4::homogenize(Float4& result) const {
    float w = v[3];
    if (w != 0){
        for (int i = 0; i < 4; i++){
            result[i] = v[i] / w;
        }
    }
}

//
// Multiply all four (X Y Z W) components of this point by scale factor,
// into result.
//
void Float4::times(float factor, Float4& result) const {
    for (int i = 0; i < 4; i++){
        
        result.v[i] = v[i] * factor;
    }
}


//
// return the distance from this point to "other".
//
float Point4::distanceTo(Point4& other) const {
    Vector4 distance = *this - other;
    return distance.length();
}

//
// Length, using three (X Y Z) components of this point.
//
float Vector4::length() const {
    return sqrt(*this * *this);
}

//
// Divide first three (X Y Z) components of this point by length,
// into result (if length==0, do nothing)
//
void Vector4::normalize(Vector4& result) const {
    float length = this->length();
    if (length != 0 ){
        length = 1 / length;
        times( length, result);
    }
}

//
// Cross product of this vector x "other", into result
// Use only X Y Z components.
// Sets W component of product to 0.
//
void Vector4::cross(const Vector4& other, Vector4& result) const {
    result.v[0] = v[1] * other.v[2] - v[2] * other.v[1];
    result.v[1] = v[2] * other.v[0] - v[0] * other.v[2];
    result.v[2] = v[0] * other.v[1] - v[1] * other.v[0];
}

//
// return the angle (in radians) between this vector and "other".
// If this or other is (0 0 0), return 0 angle.
//
float Vector4::angle(Vector4& other) const {
    if (v[0] == v[1] && v[1] == v[2] && v[2] == 0)
        return 0;
    if (other.v[0] == other.v[1] && other.v[1] == other.v[2] && other.v[2] == 0)
        return 0;
    float theta = (*this * other) / (length() * other.length());
    theta = acos(theta);
    return theta;
}

//
// Copy all components of other matrix into this.
//
void Matrix4::copyFrom(const Matrix4& other) {
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            m[r][c] = other.m[r][c];
        }
    }
}

//
// Add all components of this and "other" matrix into sum
//
void Matrix4::plus(const Matrix4& other, Matrix4& sum) const {
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            sum.m[r][c] = m[r][c] + other.m[r][c];
        }
    }
}

//
// Subtract all components of this matrix minus "other", into difference
//
void Matrix4::minus(const Matrix4& other, Matrix4& difference) const {
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            difference.m[r][c] = m[r][c] - other.m[r][c];
        }
    }
}

//
// Multiply all components by factor, into product
//
void Matrix4::times(float factor, Matrix4& product) const {
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            product.m[r][c] = m[r][c] * factor;
        }
    }
}

//
// Multiply (*this) x ("other"), and put product into product
//
// CAREFULL!! It's possible that this == &product!
// So watch for partially-modified entries.
// Store result in temp array first, THEN copy temp into product.
//
void Matrix4::times(const Matrix4& other, Matrix4 &product) const {
    Matrix4 temp;
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            float sum = 0;
            for (int j = 0; j < 4; j++)
                sum += m[r][j] * other.m[j][c];
            temp.m[r][c] = sum;
        }
    }
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            product.m[r][c] = temp.m[r][c];
        }
    }
}

//
// Multiply (*this) x ("point"), and put resulting point into "product".
// CAREFULL!! it's possible that point == product,
// so watch for partially-modified entries.
// Store result in temp array first, THEN copy temp into product.
//
void Matrix4::times(const Float4& point, Float4& product) const {
    Float4 temp;
    for (int r = 0; r < 4; r++) {
        float sum = 0;
        for (int j = 0; j < 4; j++)
            sum += m[r][j] * point[j];
        temp[r] = sum;
    }
    for (int r = 0; r < 4; r++) {
        product[r] = temp[r];
    }
}

//
// Transpose this matrix, put result into result.
//
// CAREFULL!! It's possible that this == &result!
// So watch for partially-modified entries.
// Store transposed matrix in temp array first, then copy temp into result.
//
void Matrix4::transpose(Matrix4& result) const {
    Matrix4 temp;
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            temp.m[r][c] = m[c][r];
        }
    }
    for (int r = 0; r < 4; r++) {
        for (int c = 0; c < 4; c++) {
            result.m[r][c] = temp.m[r][c];
        }
    }
}

//
// Set this matrix to identity matrix.
//
void Matrix4::setToIdentity() {
    set(1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about X axis.
// "angle" is in DEGREES.
//
void Matrix4::setToXRotation(float angle) {
    float r = angle * M_PI / 180;
    float s = sin(r);
    float c = cos(r);
    set(1,0,0,0,
        0,c,-s,0,
        0,s,c,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about Y axis.
// "angle" is in DEGREES.
//
void Matrix4::setToYRotation(float angle) {
    float r = angle * M_PI / 180;
    float s = sin(r);
    float c = cos(r);
    set(c,0,s,0,
        0,1,0,0,
        -s,0,c,0,
        0,0,0,1);
}

//
// Set this matrix to rotation about Z axis.
// "angle" is in DEGREES.
//
void Matrix4::setToZRotation(float angle) {
    float r = angle * M_PI / 180;
    float s = sin(r);
    float c = cos(r);
    set(c,-s,0,0,
        s,c,0,0,
        0,0,1,0,
        0,0,0,1);
}

//
// Set this matrix to translation matrix.
//
void Matrix4::setToTranslation(float tx, float ty, float tz) {
    set(0,0,0,tx,
        0,0,0,ty,
        0,0,0,tz,
        0,0,0,1);
}

//
// Set this matrix to scaling matrix.
//
void Matrix4::setToScaling(float sx, float sy, float sz) {
    set(sx,0,0,0,
        0,sy,0,0,
        0,0,sz,0,
        0,0,0,1);
}
