#include <iostream>
using namespace std;

int x = 7;
int y = 7;


int main() 
{


    unsigned char image[x][y] = {
        {'x', ' ', ' ', ' ', ' ', ' ', 'x'},
        {' ', 'x', ' ', ' ', ' ', ' ', 'x'},
        {' ', 'x', ' ', ' ', ' ', ' ', 'x'},
        {' ', 'x', ' ', ' ', ' ', ' ', 'x'},
        {' ', 'x', ' ', ' ', ' ', 'x', ' '},
        {' ', ' ', 'x', ' ', 'x', ' ', ' '},
        {' ', ' ', ' ', 'x', ' ', ' ', ' '},
    };

    // print initial image
    for (int i = 0; i < x; i++){
        for (int j = 0; j < y; j++){
            cout << image[i][j] << " ";
        }
        cout << endl;
    }

    cout << endl;



    // flip
    for(int j = 0; j < y; j++){
        int width = x;
        char t = image[0][j];
        for(int i = 0; i < x / 2; i++){
                image[i][j] = image[--width][j];
        }
        image[x - 1][j] = t;
    }


    // transpose 
    /*
    for(int j = 0; j < y; j++){	
        for(int i = j; i < x; i++){
            char t = image[i][j];
                image[i][j] = image[j][i];
                image[j][i] = t;
        }

    } */

    // print rotated image
    for (int i = 0; i < x; i++){
        for (int j = 0; j < y; j++){
            cout << image[i][j] << " ";
        }
        cout << endl;
    }



}


