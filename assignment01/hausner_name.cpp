//========================================================================
// A simple scene.
// Author: Alejo Hausner
// Date: Sept. 2017.
//========================================================================

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>

#include "camera.h"

using namespace std;

Camera cam;

//////////////////////////////////////////////////////////////////////
//
// Call this to redraw the scene.
//
//////////////////////////////////////////////////////////////////////

void display(void)
{
    //
    // Set the background colour to darkish blue-gray.
    //
    glClearColor(.4f,.4f, 0.6f,1.f);

    //
    // OK, now clear the screen with the background colour
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    //
    // draw the coordinate grid
    //
    cam.draw_grid(0.1, 1);

    // My initial
    glColor3d(1, 0, 0);

    glBegin(GL_POLYGON);
    glVertex2d(0,0);
    glVertex2d(0.4, 1);
    glVertex2d(0.6, 1);
    glVertex2d(0.2, 0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex2d(1,0);
    glVertex2d(0.6, 1);
    glVertex2d(0.4, 1);
    glVertex2d(0.8, 0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex2d(0.4, 0.5);
    glVertex2d(0.6, 0.5);
    glVertex2d(0.6, 0.4);
    glVertex2d(0.4, 0.4);
    glEnd();

    // My last name

    // letter "h"
    glColor3d(1, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(1.2, 0);
    glVertex2d(1.2, 1);
    glVertex2d(1.2, 0.4);
    glVertex2d(1.3, 0.5);
    glVertex2d(1.7, 0.5);
    glVertex2d(1.8, 0.4);
    glVertex2d(1.8, 0);
    glEnd();

    // letter "a"
    glColor3d(0.8, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(2.2, 0.2);
    glVertex2d(2.2, 0.4);
    glVertex2d(2.3, 0.5);
    glVertex2d(2.7, 0.5);
    glVertex2d(2.8, 0.4);
    glVertex2d(2.8, 0.5);
    glVertex2d(2.8, 0);
    glVertex2d(2.8, 0.2);
    glVertex2d(2.7, 0.0);
    glVertex2d(2.3, 0.0);
    glVertex2d(2.2, 0.2);
    glEnd();

    // letter "u"
    glColor3d(0.7, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(3.2, 0.5);
    glVertex2d(3.2, 0.2);
    glVertex2d(3.3, 0);
    glVertex2d(3.7, 0);
    glVertex2d(3.8, 0.2);
    glVertex2d(3.8, 0.5);
    glVertex2d(3.8, 0);
    glEnd();

    // letter "s"
    glColor3d(0.6, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(4.2, 0);
    glVertex2d(4.7, 0);
    glVertex2d(4.8, 0.15);
    glVertex2d(4.8, 0.2);
    glVertex2d(4.7, 0.25);
    glVertex2d(4.3, 0.25);
    glVertex2d(4.2, 0.3);
    glVertex2d(4.2, 0.35);
    glVertex2d(4.3, 0.5);
    glVertex2d(4.8, 0.5);
    glEnd();

    // letter "n"
    glColor3d(0.5, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(5.2, 0);
    glVertex2d(5.2, 0.5);
    glVertex2d(5.2, 0.4);
    glVertex2d(5.3, 0.5);
    glVertex2d(5.7, 0.5);
    glVertex2d(5.8, 0.4);
    glVertex2d(5.8, 0);
    glEnd();

    // letter "e"
    glColor3d(0.4, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(6.8, 0);
    glVertex2d(6.3, 0);
    glVertex2d(6.2, 0.2);
    glVertex2d(6.2, 0.4);
    glVertex2d(6.3, 0.5);
    glVertex2d(6.7, 0.5);
    glVertex2d(6.8, 0.4);
    glVertex2d(6.8, 0.25);
    glVertex2d(6.2, 0.25);
    glEnd();

    // letter "r"
    glColor3d(0.3, 1, 0);
    glBegin(GL_LINE_STRIP);
    glVertex2d(7.2, 0);
    glVertex2d(7.2, 0.5);
    glVertex2d(7.2, 0.4);
    glVertex2d(7.3, 0.5);
    glVertex2d(7.8, 0.5);
    glEnd();
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q ) &&
        action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

int main(int argc, char *argv[])
{
    GLFWwindow* window;
    int w = 900;
    int h = 200;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "House and Tree", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    cam = Camera(-0.5,-0.5, 9, 2, w, h, window);

    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

//! [code]
