#include "Material.h"

Material::Material() {
    ka.set(.5, .5, .5);
    kd.set(.5, .5, .5);
    ks.set(.5, .5, .5);
    n = 10;
}

Material::Material(Color& ambient, Color& diffuse, Color& specular,
                   int shininess) {
    ka = ambient;
    kd = diffuse;
    ks = specular;
    n  = shininess;
}

void Material::set(Color& ambient, Color& diffuse, Color& specular,
                   int shininess) {
    ka = ambient;
    kd = diffuse;
    ks = specular;
    n  = shininess;
}


