#if !defined(_LIGHT_H_)
#define _LIGHT_H_

#include "GeomLib.h"
#include "Color.h"

class Light {
public:
    Light();
    Light(Color& color, Point4& position);
    void set(Color& color, Point4& position);
    Color& getColor() {return c;};
    Point4& getPos() {return p;};

private:
    Point4 p;
    Color c;
};

#endif
