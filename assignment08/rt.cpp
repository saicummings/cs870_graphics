/////////////////////////////////////////////////////
//
// A simple ray tracer.
//
//////////////////////////////////////////////////////

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

#include "Camera.h"
#include "KBUI.h"

#include "GeomLib.h"
#include "Color.h"
#include "Object.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Hit.h"
#include "HitList.h"
#include "Tokenizer.h"
#include "Light.h"
#include "Material.h"

using namespace std;

KBUI the_ui;
Camera cam;

/////////////////////////////////////////////////////
// DECLARATIONS
////////////////////////////////////////////////////

// Forward declarations for functions in this file
void init_UI();
void setup_camera();
void check_for_resize();
Ray4 get_ray(int xDCS, int yDCS);
bool first_hit(Ray4 &ray, Hit& hit);
Color ray_color(int xDCS, int yDCS);

Vector4 mirror_direction(Vector4& L, Vector4& N);
Color local_illumination(Vector4& V, Vector4& N, Vector4& L,
                         Material& mat, Color& ls);
void read_scene(const char *sceneFile);

void render();
void camera_changed();
void cam_param_changed(float);
bool get_was_window_resized();
void reset_camera(float);
void init_scene();
void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods );
static void error_callback(int error, const char* description);
static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods);
void display ();
int main(int argc, char *argv[]);

typedef unsigned char byte; // In case compiler doesn't define "byte" type

// USEFUL Flag:
// When the user clicks on a pixel, the mouse_button_callback does two things:
//   1. sets this flag.
//   2. calls ray_color() on that pixel
// This lets you check all your intersection code, for ONE ray of your choosing.
bool debugOn = false;

// FRAME BUFFER Declarations.
// The initial image is 300 x 300 x 3 bytes (3 bytes per pixel)
int winWidth  = 300;
int winHeight = 300;
byte *img = NULL;   // image is allocated by check_for_resize(), not here.

// These are the camera parameters.
// The camera position and orientation:
Point4  eye;
Point4  lookat;
Vector4 vup;

// The camera's HOME parameters, used in reset_camera()
Point4  eye_home(1.4, 1.6, 4.6);
Point4  lookat_home(0, 0, 0);
Vector4 vup_home(0, 1, 0);

// The clipping frustum
float clipL = -1;
float clipR = +1;
float clipB = -1;
float clipT = +1;
float clipN =  2;

vector<Object*> scene_objects; // list of objects in the scene
vector<Light> scene_lights; // list of lights in the scene
vector<Material> materials; // list of available materials
Color ambient_light; // indirect light that shines when all lights are blocked
float ambient_fraction; // how much of lights is ambient

Matrix4 Mvcswcs;  // the inverse of the view matrix.
HitList hits;     // list of hit records for current ray
Hit *hitPool;     // array of available hit records.

// Used to trigger render() when camera has changed.
bool frame_buffer_stale = true;

// Rays which miss all objects have this color.
const Color background_color(0.3, 0.4, 0.4); // dark blue

// Shadows on/off
float show_shadows = 1.0;

//////////////////////////////////////////////////////////////////////
// Compute Mvcstowcs.
// YOU MUST IMPLEMENT THIS FUNCTION.
//////////////////////////////////////////////////////////////////////
void setup_camera() {
    //
    // The camera's basis vectors
    //
    Vector4 z = (eye - lookat).normalized();
    Vector4 x = (vup^z).normalized();
    Vector4 y = z^x;

    //
    // The inverse of the view matrix
    //
    Mvcswcs.set(x.X(), y.X(), z.X(), eye.X(),
                x.Y(), y.Y(), z.Y(), eye.Y(),
                x.Z(), y.Z(), z.Z(), eye.Z(),
                0.0,   0.0,   0.0,      1.0);

    if (debugOn) {
        cout << "Mvcs->wcs:\n";
        cout << Mvcswcs << "\n";
    }

    check_for_resize();
}

/////////////////////////////////////////////////////////
// Get color of a ray passing through (x,y)DCS.
// YOU MUST IMPLEMENT THIS FUNCTION.
//
// Parameters:
//   xDCS, yDCS : the pixel's coordinates
// Returns:
//   the color of the first illuminated surface that the ray hits
//
// YOU MUST CHANGE THIS:
//  It should return the correct color, if there is a hit.
//
/////////////////////////////////////////////////////////

Color ray_color(int xDCS, int yDCS)
{
    Ray4 ray = get_ray(xDCS, yDCS);

    if (debugOn) {
        cout << "Ray: S=" << ray.start << " V=" << ray.direction << "\n";
    }

    Hit hit;
    bool got_a_hit = first_hit(ray, hit);

    if (debugOn) {
        cout << "After first_hit, got a hit: " << got_a_hit << "\n";
        cout.flush();
    }

    if (got_a_hit) {

        if (debugOn) {
            cout << " ray(" << xDCS << " " << yDCS << ") HITS at:\n";
            cout << "P=" << hit.p << "\n";
            cout.flush();
        }

        ///////////////////////////////////////
        // CHANGE THIS COLOR:
        // It should be the sum of the contributions
        // of all the lights, plus the ambient intensity.
        ///////////////////////////////////////
        Color c(0.0, 0.0, 0.0);

        
        // glossy_color
        for(int i = 0; i < (int) scene_lights.size(); i++){
            Light l = scene_lights[i];

            //
            Ray4 shadowr = Ray4(hit.hitPoint(), l.getPos());
            Hit shadowh;
            shadowr.start = shadowr.start + (0.001 * shadowr.direction);
            got_a_hit = first_hit(shadowr, shadowh);

            if (got_a_hit && show_shadows > 0){
                continue;
            }

            Object * o = hit.getObject();
            Color lcolor = l.getColor();
            Vector4 ldir = (l.getPos() - hit.hitPoint()).normalized();
            Vector4 v = ray.direction;
            v = -v;
            c += local_illumination(v, hit.normal(), 
                ldir, (*o).get_material(), lcolor); 
            
       
        }

        

        return c;
    }
    else {
        if (debugOn) {
            cout << " ray(" << xDCS << " " << yDCS << "): NO HIT\n";
            cout.flush();
        }

        return background_color;
    }

}

/////////////////////////////////////////////////////////
// Initialize a ray starting at (x y)DCS.
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////

Ray4 get_ray(int xDCS, int yDCS) {
    Point4 p_dcs(xDCS, yDCS, 0);

    /* DCS to VCS */
    float dx = (clipR - clipL) / winWidth;
	float dy = (clipT - clipB) / winHeight;
    float x_vcs = clipL + (p_dcs.X() + 0.5) * dx;
	float y_vcs = clipB + (p_dcs.Y() + 0.5) * dy;
    float z_vcs = -clipN;
    Point4 p_vcs(x_vcs, y_vcs, z_vcs); /* setup_camera(); */
    Float4 v_wcs = Mvcswcs * p_vcs;
    Point4 p_wcs(v_wcs.X(), v_wcs.Y(), v_wcs.Z());
    Vector4 v = (p_wcs - eye).normalized();

    /*
    Point4 start(0,0,0);
    Vector4 direction(0,0,-1);
    */

    return Ray4(p_wcs, v);
}

/////////////////////////////////////////////////////////
// Find the first object hit by the ray, if any
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////

bool first_hit(Ray4 &ray, Hit& hit) {
    if (debugOn) {
        cout << "entering first_hit. scene_objects: " << (int)scene_objects.size() << endl;
        cout.flush();
    }

    Hit first;
    bool hr = false;
    first.dist = 0x7FFF;

    for (int i = 0; i < (int)scene_objects.size(); i++){
        Hit h;
        if (scene_objects[i]->intersects(ray, h)){
            if (h.dist < first.dist){
                first = h;
                hr = true;
            }
        }
    }

    if (hr)
        hit = first;
    
    return hr;
}

/////////////////////////////////////////////////////////
// Reflection direction for a ray hitting a mirror surface
//
// Parameters:
//  L : unit vector towards light source
//  N : unit vector normal to surface
// Returns:
//  R = 2(N dot L)N - L
/////////////////////////////////////////////////////////

Vector4 mirror_direction(Vector4& L, Vector4& N) {
    return 2*(N.dot(L))*N - L;
}

/////////////////////////////////////////////////////////
// Compute one light's contribution to the color a surface
//
// Parameters:
//   V : unit vector, towards eye (ray's direction, reversed)
//   N : unit vector, normal to the surface, at the hit point
//   L : unit vector, in direction from hit point to a light source
//   mat : the surface's material properties
//   ls : the light's color
//
// Returns:
//   Color of surface, using Phong's equation for local illumination:
//   I = Id + Is, where
//     Id = ls * kd * N dot L
//     Is = ls * ks * (R dot V)^n
//
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////
Color local_illumination(Vector4& V, Vector4& N, Vector4& L,
                         Material& mat, Color& ls) {
    float NL = N.dot(L);
    if (NL > 0) {
        // light on up side of surface.
        
        Vector4 R = mirror_direction(L, N);
        float RV = R.dot(V);
        if (RV < 0) {
            // On back side of specular blob, use 0 instead.
            RV = 0;
        }

        //
        // Fast (R.V)^n
        // Avoid pow().  It's SLOOOOWW!
        //
        float RVn = 1.0f;

        if (debugOn)
        {
          cout << "R=" << R << endl;
          cout << "V=" << V << endl;
          cout << "RV=" << RV << endl;
        }

        if (RV <= 0.0f)
            RVn = 0.0f;
        else {
            int n = mat.getShininess();
            while (n > 0) {
                if (n & 1)
                    RVn *= RV;
                RV *= RV;
                n >>= 1;
            }
        }

        Color& kd = mat.getDiffuse();
        Color& ks = mat.getSpecular();

        if (debugOn) {
            cout << "ls: " << ls << " kd: " << kd << "\n";
            cout << "ks: " << ks << " NL: " << NL << " RVn:" << RVn << "\n";
        }

        return ls * (kd * NL + ks * RVn);
    }
    else
        return Color(0,0,0);  // light on back, no illumination.
}

/////////////////////////////////////////////////////////
// This function actually generates the ray-traced image.
//
// You don't have to change this function.
/////////////////////////////////////////////////////////

void render() {
    int x,y;
    byte r,g,b;
    int p;

    setup_camera();

    ambient_light = Color(0,0,0);
    for (auto light : scene_lights) {
        ambient_light += light.getColor() * ambient_fraction;
    }

    for (y=0; y<winHeight; y++) {
        for (x=0; x<winWidth; x++) {

            if (debugOn) {
                cout << "pixel (" << x << " " << y << ")\n";
                cout.flush();
            }

            Color pixel_color = ray_color(x, y);
            pixel_color.clamp();

            // shadow ray
            

            r = (byte) (pixel_color.R() * 255.0);
            g = (byte) (pixel_color.G() * 255.0);
            b = (byte) (pixel_color.B() * 255.0);

            p = (y*winWidth + x) * 3;

            img[p]   = r;
            img[p+1] = g;
            img[p+2] = b;
        }
    }
}

//////////////////////////////////////////////////////////////////////
// If window size has changed, re-allocate the frame buffer
//
// You don't have to change this function.
//////////////////////////////////////////////////////////////////////

void check_for_resize() {
    // Now, check if the frame buffer needs to be created,
    // or re-created.

    bool should_allocate = false;
    if (img == NULL) {
        // frame buffer not yet allocated.
        should_allocate = true;
    }
    else if (winWidth  != cam.get_win_W() ||
             winHeight != cam.get_win_H()) {

        // frame buffer allocated, but has changed size.
        delete[] img;
        should_allocate = true;
        winWidth  = cam.get_win_W();
        winHeight = cam.get_win_H();
    }

    if (should_allocate) {

        if (debugOn) {
            cout << "ALLOCATING: (W H)=(" << winWidth
                 << " " << winHeight << ")\n";
        }

        img = new byte[winWidth * winHeight * 3];
        camera_changed();
    }
}

//////////////////////////////////////////////////////
//
// Displays, on STDOUT, the colour of the pixel that
//  the user clicked on.
//
// THIS IS VERY USEFUL!  USE IT!
//
// You don't have to change this function.
//
//////////////////////////////////////////////////////

void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods )
{
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;

    if (action == GLFW_PRESS)
    {
        debugOn = true;

        // Get the mouse's position.

        double xpos, ypos;
        int W, H;
        glfwGetCursorPos(window, &xpos, &ypos);
        glfwGetWindowSize(window, &W, &H);

        // mouse position, as a fraction of the window dimensions
        // The y mouse coord increases as you move down,
        // but our yDCS increases as you move up.
        double mouse_fx = xpos / float(W);
        double mouse_fy = (W - 1 - ypos) / float(W);

        int xDCS = (int)(mouse_fx * winWidth + 0.5);
        int yDCS = (int)(mouse_fy * winHeight + 0.5);

        Color pixelColor = ray_color(xDCS, yDCS);

        if (debugOn) {
            cout << "cursorpos:" << xpos << " " << ypos << "\n";
            cout << "Width Height: " << winWidth << " " << winHeight << "\n";
            cout << "Window Size: " << W << " " << H << "\n";
            cout << "Pixel at (x y)=(" << xDCS << " " << yDCS << ")\n";
            cout << "Pixel Color = " << pixelColor << endl;
        }
        debugOn = false;
    }
}

/////////////////////////////////////////////////////////
// Call this when scene has changed, and we need to re-run
// the ray tracer.
// You don't have to change this function.
/////////////////////////////////////////////////////////

void camera_changed() {
    float dummy=0;
    cam_param_changed(dummy);
}

/////////////////////////////////////////////////////////
// Called when user modifies a camera parameter.
// You don't have to change this function.
/////////////////////////////////////////////////////////
void cam_param_changed(float param) {
    frame_buffer_stale = true;
}

/////////////////////////////////////////////////////////
// Check if window was resized.
// You don't have to change this function.
/////////////////////////////////////////////////////////

bool get_was_window_resized() {
    int new_W = cam.get_win_W();
    int new_H = cam.get_win_H();

    if (new_W != winWidth || new_H != winHeight) {
        camera_changed();
        winWidth = new_W;
        winHeight = new_H;
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////
// This function sets up a simple scene.
//
// YOU MUST IMPLEMENT THIS FUNCTION.
// See the documentation to learn the format of the data file.
/////////////////////////////////////////////////////
void read_scene(const char *filename) {
    ambient_light.set(0,0,0);
    int nMaterials;
    int nObjects;
    int nLights;

    float x, y, z, n, r, g, b;

    Tokenizer toker(filename);

    while (!toker.eof()) {
        string keyword = toker.next_string();

        if (keyword == string("")) {
            continue; // skip blank lines

        } else if (keyword == string("#materials")) {
            nMaterials = toker.next_number();
            materials.reserve(nMaterials);

        } else if (keyword == string("#light")) {
            nLights = toker.next_number();
            scene_lights.reserve(nLights);

        } else if (keyword == string("#objects")) {
            nObjects = toker.next_number();
            scene_objects.reserve(nObjects);
            
        } else if (keyword == string("camera_eye")) {
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
            eye = Point4 (x, y ,z);
            cout << keyword << " " << x << " " << y << " " << z << endl;

        } else if (keyword == string("camera_lookat")) {
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
            lookat = Point4 (x, y ,z);
            cout << keyword << " " << x << " " << y << " " << z << endl;

        } else if (keyword == string("camera_vup")) {
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
            vup = Vector4 (x, y ,z);
            cout << keyword << " " << x << " " << y << " " << z << endl;

        } else if (keyword == string("camera_clip")) {
            clipL = toker.next_number();
            clipR = toker.next_number();
            clipB = toker.next_number();
            clipT = toker.next_number();
            clipN = toker.next_number();

            cout << keyword << " " << clipL << " " << clipR 
                << " " << clipB << " " << clipT << " " << clipN << endl;

        } else if (keyword == string("material")) {
            cout << keyword << endl;
            // ambient
            keyword = toker.next_string();
            r = toker.next_number();
            g = toker.next_number();
            b = toker.next_number();
			Color a(r, g, b);
            cout << keyword << " " << r << " " << g << " " << " " << b << endl;

            // diffuse
            keyword = toker.next_string();
            r = toker.next_number();
            g = toker.next_number();
            b = toker.next_number();
			Color d(r, g, b);
            cout << keyword << " " << r << " " << g << " " << " " << b << endl;

            // specular
            keyword = toker.next_string();
            r = toker.next_number();
            g = toker.next_number();
            b = toker.next_number();
			Color s(r, g, b);
            cout << keyword << " " << r << " " << g << " " << " " << b << endl;

            // shininess
            keyword = toker.next_string();
            n = toker.next_number();
            cout << keyword << " " << n << endl;


			materials.push_back(Material(a, d, s, n));

        } else if (keyword == string("light")) {
            cout << keyword << endl;

            keyword = toker.next_string();
            r = toker.next_number();
            g = toker.next_number();
            b = toker.next_number();
			Color c(r, g, b);
            cout << keyword << " " << r << " " << g << " " << " " << b << endl;

            keyword = toker.next_string();
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
			Point4 p(x, y, z);
            cout << keyword << " " << x << " " << y << " " << " " << z << endl;

            scene_lights.push_back(Light(c, p));

        } else if (keyword == string("triangle")) {
            cout << keyword << endl;

            keyword = toker.next_string();
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
			Point4 p1(x, y, z);
            cout << keyword << " " << x << " " << y << " " << " " << z << endl;

            keyword = toker.next_string();
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
			Point4 p2(x, y, z);
            cout << keyword << " " << x << " " << y << " " << " " << z << endl;

            keyword = toker.next_string();
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
			Point4 p3(x, y, z);
            cout << keyword << " " << x << " " << y << " " << " " << z << endl;

			keyword = toker.next_string();
            n = toker.next_number();
            cout << keyword << " " << n << endl;

			scene_objects.push_back(new Triangle(p1, p2, p3, materials.at(n)));

        } else if (keyword == string("sphere")) {
            cout << keyword << endl;

            keyword = toker.next_string();
            x = toker.next_number();
            y = toker.next_number();
            z = toker.next_number();
			Point4 c(x, y, z);
            cout << keyword << " " << x << " " << y << " " << " " << z << endl;

            keyword = toker.next_string();
            r = toker.next_number();
            cout << keyword << " " << r << endl;

            keyword = toker.next_string();
            n = toker.next_number();
            cout << keyword << " " << n << endl;

			scene_objects.push_back(new Sphere(c, r, materials.at(n)));

        } else {
            cerr << "Parse error: unrecognized keyword \""
                 << keyword << "\"\n";
            //exit(EXIT_FAILURE);
        }
    }
}

///////////////////////////////////////////////////
// Resets the camera parameters.
// You don't have to change this function.
///////////////////////////////////////////////////

void reset_camera(float dummy) {
    eye = eye_home;
    lookat = lookat_home;
    vup = vup_home;

    camera_changed();
}


/////////////////////////////////////////////////////////
// Called on a GLFW error.
// You don't have to change this function.
/////////////////////////////////////////////////////////

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

//////////////////////////////////////////////////////
// Quit if the user hits "q" or "ESC".
// All other key presses are passed to the UI.
// You don't have to change this function.
//////////////////////////////////////////////////////

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if (key == GLFW_KEY_Q ||
        key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    else if (action == GLFW_RELEASE) {
        the_ui.handle_key(key);
    }
}


//////////////////////////////////////////////////////
// Show the image.
// You don't have to change this function.
//////////////////////////////////////////////////////

void display () {
    glClearColor(.1f,.1f,.1f, 1.f);   /* set the background colour */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    glRasterPos3d(0.0, 0.0, 0.0);

    glPixelStorei(GL_PACK_ALIGNMENT,1);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    if (frame_buffer_stale) {
        //
        // Don't re-render the scene EVERY time display() is called.
        // It might get called if the window is moved, or if it
        // is exposed.  Only re-render if the window is RESIZED.
        // Resizing triggers a call to handleReshape, which sets
        // frameBufferStale.
        //
        render();
        frame_buffer_stale = false;
    }

    //
    // This paints the current image buffer onto the screen.
    //
    glDrawPixels(winWidth,winHeight,
                 GL_RGB,GL_UNSIGNED_BYTE,img);

    glFlush();
}

///////////////////////////////////////////////////////////////////
// Set up the keyboard UI.
// No need to change this.
//////////////////////////////////////////////////////////////////
void init_UI() {
    // These variables will trigger a call-back when they are changed.
    the_ui.add_variable("Eye X", &eye.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Eye Y", &eye.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Eye Z", &eye.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Shadows", &show_shadows, 0, 1, 1, cam_param_changed);
    the_ui.add_variable("Ambient Fraction", &ambient_fraction, 0, 1, 0.1,
        cam_param_changed);

    the_ui.add_variable("Ref X", &lookat.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Ref Y", &lookat.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Ref Z", &lookat.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Vup X", &vup.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Vup Y", &vup.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Vup Z", &vup.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Clip L", &clipL,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip R", &clipR,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip B", &clipB,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip T", &clipT,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip N", &clipN,   -10, 10, 0.2, cam_param_changed);

    float dummy2=0;
    the_ui.add_variable("Reset Camera", &dummy2,0,100, 0.001, reset_camera);

    the_ui.done_init();

}

//////////////////////////////////////////////////////
// Main program.
// You don't have to change this function.
//////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    init_UI();
    if (argc < 2) {
        cerr << "Usage:\n";
        cerr << "  rt <scene-file.txt>\n";
        exit(EXIT_FAILURE);
    }

    read_scene(argv[1]);

    GLFWwindow* window;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(winWidth, winHeight,
                              "Ray Traced Scene", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    int w = winWidth;
    int h = winHeight;

    cam = Camera(0,0, w,h, w, h, window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    float dummy=0;
    reset_camera(dummy);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();
        setup_camera();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

//! [code]

