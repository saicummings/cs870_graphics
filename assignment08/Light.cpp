#include "Light.h"

Light::Light() {
    p.set(0,0,0);
}

Light::Light(Color& color, Point4& position) {
    c = color;
    p = position;
}

void Light::set(Color& color, Point4& position) {
    c = color;
    p = position;
}
