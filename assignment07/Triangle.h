#if !defined(_TRIANGLE_H_)

#define _TRIANGLE_H_

#include "Object.h"
#include "GeomLib.h"
#include "Hit.h"

class Triangle : public virtual Object {
public:
    Triangle(const Point4& v1, const Point4& v2, const Point4& v3,
             const Color& color);
    bool intersects(const Ray4& ray, Hit& hit);
    bool intersects2(const Ray4& ray, Hit& hit);

private:
    Point4 v1,v2,v3;
};

#endif
