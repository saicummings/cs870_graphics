#include "Color.h"

Color::Color() {
    set(0.5, 0.5, 0.5);
}

Color::Color(float red, float green, float blue) {
    set(red, green, blue);
}

void Color::set(float red, float green, float blue) {
    v[0] = red;
    v[1] = green;
    v[2] = blue;
}

// Named access to the four components: L-values

float& Color::R() {
    return v[0];
}

float& Color::G() {
    return v[1];
}

float& Color::B() {
    return v[2];
}

// Named access to the components: R-values

const float& Color::R() const {
    return v[0];
}

const float& Color::G() const {
    return v[1];
}

const float& Color::B() const {
    return v[2];
}

