#if !defined(_COLOR_H_)
#define _COLOR_H_

#include "GeomLib.h"

class Color : public Float4 {
public:
    Color();
    Color(float red, float green, float blue);
    void set(float red, float green, float blue);

    // Named access to the three components: L-values

    float& R();
    float& G();
    float& B();

    // Named access to the components: R-values

    const float& R() const;
    const float& G() const;
    const float& B() const;
};

#endif
