/////////////////////////////////////////////////////
//
// A simple ray tracer.
//
//////////////////////////////////////////////////////

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

#include "Camera.h"
#include "KBUI.h"

#include "GeomLib.h"
#include "Color.h"
#include "Object.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Hit.h"

using namespace std;

KBUI the_ui;
Camera cam;

/////////////////////////////////////////////////////
// DECLARATIONS
////////////////////////////////////////////////////

// Forward declarations for functions in this file
void init_UI();
void setup_camera();
void check_for_resize();
void setRay(int xDCS, int yDCS, Ray4& ray);
bool first_hit(Ray4 &ray, Hit& hit);
Color rayColor(int xDCS, int yDCS);
void render();
void camera_changed();
void cam_param_changed(float);
bool get_was_window_resized();
void reset_camera(float);
void init_scene();
void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods );
static void error_callback(int error, const char* description);
static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods);
void display ();
int main(int argc, char *argv[]);

typedef unsigned char byte; // In case compiler doesn't define "byte" type

// USEFUL Flag:
// When the user clicks on a pixel, the mouse_button_callback does two things:
//   1. sets this flag.
//   2. calls ray_color() on that pixel
// This lets you check all your intersection code, for ONE ray of your choosing.
bool debugOn = false;

// FRAME BUFFER Declarations.
// The initial image is 300 x 300 x 3 bytes (3 bytes per pixel)
int winWidth  = 300;
int winHeight = 300;
byte *img = NULL;   // image is allocated by check_for_resize(), not here.

// These are the camera parameters.
// The camera position and orientation:
Point4  eye;
Point4  lookat;
Vector4 vup;

// The camera's HOME parameters, used in reset_camera()
Point4  eye_home(1.4, 1.6, 4.6);
Point4  lookat_home(0, 0, 0);
Vector4 vup_home(0, 1, 0);

// The clipping frustum
float clipL = -1;
float clipR = +1;
float clipB = -1;
float clipT = +1;
float clipN =  2;

vector<Object*> sceneObjects; // list of objects in the scene

Matrix4 Mvcswcs;  // the inverse of the view matrix.
Hit *hitPool;     // array of available hit records.

// Used to trigger render() when camera has changed.
bool frame_buffer_stale = true;

// Rays which miss all objects will have this color.
Color background_color(0.3, 0.4, 0.4); // dark blue


//////////////////////////////////////////////////////////////////////
// Compute Mvcstowcs.
// YOU MUST IMPLEMENT THIS FUNCTION.
//////////////////////////////////////////////////////////////////////

void setup_camera() {
    /* These are the basis vectors */
    Vector4 z = (eye - lookat).normalized();
    Vector4 x = (vup ^ z).normalized();
    Vector4 y = z ^ x;

    Mvcswcs = Matrix4(
        x.X(), y.X(), z.X(), eye.X(),
		x.Y(), y.Y(), z.Y(), eye.Y(),
		x.Z(), y.Z(), z.Z(), eye.Z(),
		    0,     0,     0,      1);

    // PLEASE LEAVE THIS LINE IN PLACE.
    // It triggers allocation of the frame buffer.
    check_for_resize();


}

/////////////////////////////////////////////////////////
// Get color of a ray passing through (x,y)DCS.
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////

Color rayColor(int xDCS, int yDCS)
{
    Ray4 r;
    Hit hit; 
	setRay(xDCS, yDCS, r);
    if (first_hit(r, hit)){
        return hit.obj->color;
    } else {
        return background_color;
    }
}

/////////////////////////////////////////////////////////
// Initialize a ray starting at (x y)DCS.
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////

void setRay(int xDCS, int yDCS, Ray4& ray) {
    Point4 p_dcs(xDCS, yDCS, 0);

    /* DCS to VCS */
    float dx = (clipR - clipL) / winWidth;
	float dy = (clipT - clipB) / winHeight;
    float x_vcs = clipL + (p_dcs.X() + 0.5) * dx;
	float y_vcs = clipB + (p_dcs.Y() + 0.5) * dy;
    float z_vcs = -clipN;
    Point4 p_vcs(x_vcs, y_vcs, z_vcs); /* setup_camera(); */
    Float4 v_wcs = Mvcswcs * p_vcs;
    Point4 p_wcs(v_wcs.X(), v_wcs.Y(), v_wcs.Z());
    Vector4 v = (p_wcs - eye).normalized();
    ray = Ray4(p_wcs, v);
}

/////////////////////////////////////////////////////////
// Find the first object hit by the ray, if any
// YOU MUST IMPLEMENT THIS FUNCTION.
/////////////////////////////////////////////////////////

bool first_hit(Ray4 &ray, Hit& hit) {
    Hit first;
    bool hr = false;
    first.dist = 0x7FFF;

    for (int i = 0; i < (int)sceneObjects.size(); i++){
        Hit h;
        if (sceneObjects[i]->intersects(ray, h)){
            if (h.dist < first.dist){
                first = h;
                hr = true;
            }
        }
    }

    if (hr)
        hit = first;
    
    return hr;
}

/////////////////////////////////////////////////////////
// This function actually generates the ray-traced image.
// You don't have to change this function.
/////////////////////////////////////////////////////////

void render() {
    int x,y;
    byte r,g,b;
    int p;

    // Compute the Mvcs2wcs matrix.
    setup_camera();

    // Get every pixel's color:
    for (y=0; y<winHeight; y++) {
        for (x=0; x<winWidth; x++) {

            if (debugOn) {
                cout << "pixel (" << x << " " << y << ")\n";
                cout.flush();
            }

            // Ray color does the ray tracing.
            Color pixelColor = rayColor(x, y);

            // Color stores 3 floats.  Scale them to 3 bytes.
            r = (byte) (pixelColor.R() * 255.0);
            g = (byte) (pixelColor.G() * 255.0);
            b = (byte) (pixelColor.B() * 255.0);

            // Store the 3 bytes into the frame buffer.
            p = (y*winWidth + x) * 3;

            img[p]   = r;
            img[p+1] = g;
            img[p+2] = b;
        }
    }
}

//////////////////////////////////////////////////////
//
// Displays, on STDOUT, the colour of the pixel that
//  the user clicked on.
//
// THIS IS VERY USEFUL!  USE IT!
//
// You don't have to change this function.
//
//////////////////////////////////////////////////////

void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods )
{
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;

    if (action == GLFW_PRESS)
    {
        debugOn = true;

        // Get the mouse's position.

        double xpos, ypos;
        int W, H;
        glfwGetCursorPos(window, &xpos, &ypos);
        glfwGetWindowSize(window, &W, &H);

        // mouse position, as a fraction of the window dimensions
        // The y mouse coord increases as you move down,
        // but our yDCS increases as you move up.
        double mouse_fx = xpos / float(W);
        double mouse_fy = (W - 1 - ypos) / float(W);

        int xDCS = (int)(mouse_fx * winWidth + 0.5);
        int yDCS = (int)(mouse_fy * winHeight + 0.5);

        Color pixelColor = rayColor(xDCS, yDCS);

        if (debugOn) {
            cout << "Pixel at (x y)=(" << xDCS << " " << yDCS << ")\n";
            cout << "Pixel Color = " << pixelColor << endl;
        }
        debugOn = false;
    }
}

//////////////////////////////////////////////////////////////////////
// If window size has changed, re-allocate the frame buffer
// You don't have to change this function.
//////////////////////////////////////////////////////////////////////

void check_for_resize() {
    // Now, check if the frame buffer needs to be created,
    // or re-created.

    bool should_allocate = false;
    if (img == NULL) {
        // frame buffer not yet allocated.
        should_allocate = true;
    }
    else if (winWidth  != cam.get_win_W() ||
             winHeight != cam.get_win_H()) {

        // frame buffer allocated, but has changed size.
        delete[] img;
        should_allocate = true;
        winWidth  = cam.get_win_W();
        winHeight = cam.get_win_H();
    }

    if (should_allocate) {

        if (debugOn) {
            cout << "ALLOCATING: (W H)=(" << winWidth
                 << " " << winHeight << ")\n";
        }

        img = new byte[winWidth * winHeight * 3];
        camera_changed();
    }
}

/////////////////////////////////////////////////////////
// Call this when scene has changed, and we need to re-run
// the ray tracer.
// You don't have to change this function.
/////////////////////////////////////////////////////////

void camera_changed() {
    float dummy=0;
    cam_param_changed(dummy);
}

/////////////////////////////////////////////////////////
// Called when user modifies a camera parameter.
// You don't have to change this function.
/////////////////////////////////////////////////////////
void cam_param_changed(float param) {
    frame_buffer_stale = true;
}

/////////////////////////////////////////////////////////
// Check if window was resized.
// You don't have to change this function.
/////////////////////////////////////////////////////////

bool get_was_window_resized() {
    int new_W = cam.get_win_W();
    int new_H = cam.get_win_H();

    if (new_W != winWidth || new_H != winHeight) {
        camera_changed();
        winWidth = new_W;
        winHeight = new_H;
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////
// This function sets up a simple scene.
// You don't have to change this function.
/////////////////////////////////////////////////////
void init_scene() {
    sceneObjects.push_back(new Sphere(Point4(0,1,0), 0.5, Color(1, 0.5, 0.5)));
    sceneObjects.push_back(new Sphere(Point4(0,0,0), 0.75, Color(0.5, 1, 0.5)));
    sceneObjects.push_back(new Sphere(Point4(0,-1.5,0), 1, Color(0.5, 0.5, 1)));

    sceneObjects.push_back(new Triangle(Point4( 4, -2.5, -4),
                                        Point4(-4, -2.5,  4),
                                        Point4( 4, -2.5,  4),
                                        Color(1, 0.75, 0.5)));
    sceneObjects.push_back(new Triangle(Point4(-4, -2.5, -4),
                                        Point4(-4, -2.5,  4),
                                        Point4( 4, -2.5, -4),
                                        Color(1, 0.75, 0.5)));

    hitPool = new Hit[20];
}

///////////////////////////////////////////////////
// Resets the camera parameters.
// You don't have to change this function.
///////////////////////////////////////////////////

void reset_camera(float dummy) {
    eye = eye_home;
    lookat = lookat_home;
    vup = vup_home;

    clipL = -1;
    clipR = +1;
    clipB = -1;
    clipT = +1;
    clipN =  2;

    camera_changed();
}

/////////////////////////////////////////////////////////
// Called on a GLFW error.
// You don't have to change this function.
/////////////////////////////////////////////////////////

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

//////////////////////////////////////////////////////
// Quit if the user hits "q" or "ESC".
// All other key presses are passed to the UI.
// You don't have to change this function.
//////////////////////////////////////////////////////

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if (key == GLFW_KEY_Q ||
        key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    else if (action == GLFW_RELEASE) {
        the_ui.handle_key(key);
    }
}


//////////////////////////////////////////////////////
// Show the image.
// You don't have to change this function.
//////////////////////////////////////////////////////

void display () {
    glClearColor(.1f,.1f,.1f, 1.f);   /* set the background colour */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    glRasterPos3d(0.0, 0.0, 0.0);

    glPixelStorei(GL_PACK_ALIGNMENT,1);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    if (frame_buffer_stale) {
        //
        // Don't re-render the scene EVERY time display() is called.
        // It might get called if the window is moved, or if it
        // is exposed.  Only re-render if the window is RESIZED.
        // Resizing triggers a call to handleReshape, which sets
        // frameBufferStale.
        //
        render();
        frame_buffer_stale = false;
    }

    //
    // This paints the current image buffer onto the screen.
    //
    glDrawPixels(winWidth,winHeight,
                 GL_RGB,GL_UNSIGNED_BYTE,img);

    glFlush();
}

///////////////////////////////////////////////////////////////////
// Set up the keyboard UI.
// No need to change this.
//////////////////////////////////////////////////////////////////
void init_UI() {
    // These variables will trigger a call-back when they are changed.
    the_ui.add_variable("Eye X", &eye.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Eye Y", &eye.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Eye Z", &eye.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Ref X", &lookat.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Ref Y", &lookat.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Ref Z", &lookat.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Vup X", &vup.X(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Vup Y", &vup.Y(), -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Vup Z", &vup.Z(), -10, 10, 0.2, cam_param_changed);

    the_ui.add_variable("Clip L", &clipL,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip R", &clipR,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip B", &clipB,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip T", &clipT,   -10, 10, 0.2, cam_param_changed);
    the_ui.add_variable("Clip N", &clipN,   -10, 10, 0.2, cam_param_changed);

    float dummy2=0;
    the_ui.add_variable("Reset Camera", &dummy2,0,100, 0.001, reset_camera);

    the_ui.done_init();

}

//////////////////////////////////////////////////////
// Main program.
// You don't have to change this function.
//////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    init_UI();
    init_scene();

    GLFWwindow* window;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(winWidth, winHeight,
                              "Ray Traced Scene", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    int w = winWidth;
    int h = winHeight;

    cam = Camera(0,0, w,h, w, h, window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    float dummy=0;
    reset_camera(dummy);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();
        setup_camera();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

//! [code]

