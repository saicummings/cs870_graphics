#include "Triangle.h"

#include <math.h>
#include <fstream>
#include <iostream>
using namespace std;

extern bool debugOn;

Triangle::Triangle(const Point4& v1, const Point4& v2, const Point4& v3,
                   const Color& color) : Object(color) {
    this->v1 = v1;
    this->v2 = v2;
    this->v3 = v3;

}

bool Triangle::intersects(const Ray4& ray, Hit& hit) {

    Point4 s = ray.start;
	Vector4 v = ray.direction;
    float a = v.X();
    float b = v1.X() - v2.X();
    float c = v1.X() - v3.X();
    float d = v.Y();
    float e = v1.Y() - v2.Y();
    float f = v1.Y() - v3.Y();
    float g = v.Z();
    float h = v1.Z() - v2.Z();
    float j = v1.Z() - v3.Z();
    float k = v1.X() - s.X();
    float l = v1.Y() - s.Y();
    float m = v1.Z() - s.Z();

    float det = Matrix4::det3x3(a, b, c, d, e, f, g, h, j);
    float t = Matrix4::det3x3(k, b, c, l, e, f, m, h, j) / det;
	float u = Matrix4::det3x3(a, k, c, d, l, f, g, m, j) / det;
	float vi = Matrix4::det3x3(a, b, k, d, e, l, g, h, m) / det;

    if (0 <= u && u <= 1 
            && 0 <= vi && vi <= 1 
            && 0 <= u + vi && u + vi <= 1 
            && 0 <= t) {
        
         Point4 pt = v1 + u * (v2 - v1) + vi * (v3 -v1);
         hit.p = pt;
         hit.obj = this;
         hit.dist = t;
         return true;

    }

    return false;
}


