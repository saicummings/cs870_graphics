#include "Sphere.h"
#include <math.h>

#include <fstream>
#include <iostream>
using namespace std;

extern bool debugOn;

Sphere::Sphere(const Point4& center, float radius,
               const Color& color) : Object(color) {
    c = center;
	r = radius;
}

bool Sphere::intersects(const Ray4& ray, Hit& hit) {

    Point4 s = ray.start;
	Vector4 v = ray.direction;
    float bi = 2 * v * (s - c);
    float ai = v * v;
    float ci = (s - c) * (s - c) - r * r;
    float di = bi * bi - (4 * ai * ci);

    if (di <= 0)
        return false;

    float t;
    float t1 = (-bi - sqrt(di))/(2 * ai);
    float t2 = (-bi + sqrt(di))/(2 * ai);

    if (t1 > 0)
        t = t1;
    else if (t2 > 0)
        t = t2;
    else
        return false;

    Point4 ps = s + t * v;
    hit.p = ps;
    hit.obj = this;
    hit.dist = t;

    return true;
}
