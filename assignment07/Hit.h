#if !defined(_HIT_H_)
#define _HIT_H_

#include "GeomLib.h"

class Object;

class Hit {
public:
    Hit();
    Hit(Point4& hitpoint, Object* obj, double d);
    void set(Point4& hitpoint, Object* obj, double d);
    Point4& hitPoint() {return p;};
    Object* getObject() {return obj;};
    double getDistance() {return dist;};

    // private:
    Point4 p;
    Object* obj;
    double dist;
};

#endif
