#if !defined(_OBJECT_H_)

#define _OBJECT_H_

#include "Hit.h"
#include "Color.h"
#include "Object.h"
#include "GeomLib.h"

enum ObjectType {NO_OBJECT, SPHERE, TRIANGLE};

class Object {
public:
    Object(const Color& newColor);
    virtual bool intersects(const Ray4& ray, Hit& hit) = 0;
    Color& getColor() { return color; };

    Color color;

};

#endif
