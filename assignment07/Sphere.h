#if !defined(_SPHERE_H_)

#define _SPHERE_H_

#include "Object.h"
#include "GeomLib.h"
#include "Color.h"
#include "Hit.h"

class Sphere : public virtual Object {
public:
    Sphere(const Point4& center, float radius, const Color& color);
    bool intersects(const Ray4& ray, Hit& hit);

private:
    Point4 c;
    float r;
};

#endif
