//========================================================================
// A simple scene.
// Author: Sai Lekyang
// Date: 08-29-18
//========================================================================

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>

#include "camera.h"

using namespace std;

Camera cam;

//////////////////////////////////////////////////////////////////////
//
// Call this to redraw the scene.
//
//////////////////////////////////////////////////////////////////////

float bez(float p1, float p2, float p3, float p4, float t){
    float f1 = (1-t)*(1-t)*(1-t);
    float f2 = 3*t*(1-t)*(1-t);
    float f3 = 3*t*t*(1-t);
    float f4 = t*t*t;

    return f1*p1+f2*p2+f3*p3+f4*p4;
}


void display(void)
{
    //
    // Set the background colour to darkish blue-gray.
    //
    glClearColor(.4f,.4f, 0.6f,1.f);

    //
    // OK, now clear the screen with the background colour
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    //
    // draw the coordinate grid
    //
    cam.draw_grid(0.1, 1);

    // My initial
    glColor3d(0.7, 1, 0);



    float x1, x2, x3, x4, y1, y2, y3, y4;
    float x, y;
    

    // S

    glBegin(GL_LINE_STRIP);
    x1 = 0; y1 = 0;
    x2 = 1.2; y2 = 0.6;
    x3 = 0.6; y3 = 1.2;
    x4 = 0.4; y4 = 0.8;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    x1 = 0.4; y1 = 0.8;
    x2 = 0.2; y2 = 0.4;
    x3 = 0.9; y3 = 0.6;
    x4 = 0.9; y4 = 0.3;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    x1 = 0.9; y1 = 0.3;
    x2 = 0.9; y2 = 0.0;
    x3 = 0.3; y3 = -0.2;
    x4 = 0.2; y4 = 0.4;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

    // Begining of L

    glBegin(GL_LINE_STRIP);
    x1 = 1.0; y1 = 0.7;
    x2 = 2.0; y2 = 0.7;
    x3 = 1.2; y3 = 1.4;
    x4 = 1.3; y4 = 0.5;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    x1 = 1.3; y1 = 0.5;
    x2 = 1.4; y2 = -0.4;
    x3 = 0.7; y3 = 0.2;
    x4 = 1.3; y4 = 0.1;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    x1 = 1.3; y1 = 0.1;
    x2 = 1.9; y2 = 0.0;
    x3 = 2.0; y3 = -0.05;
    x4 = 2.0; y4 = 0.1;
    for (float i = 0.0; i <= 1; i += 1.0/1000000){
        x = bez(x1, x2, x3, x4, i);
        y = bez(y1, y2, y3, y4, i);
        glVertex2d(x , y);
    }
    glEnd();

}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q ) &&
        action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

int main(int argc, char *argv[])
{
    GLFWwindow* window;
    int w = 900;
    int h = 200;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "Slekyang", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    cam = Camera(-0.5,-0.5, 9, 2, w, h, window);

    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

//! [code]
