#include "Util.h"
#include <glad/glad.h>

Float4 Util::red    = Float4(1,0,0,1);
Float4 Util::green  = Float4(0,1,0,1);
Float4 Util::blue   = Float4(0,0,1,1);
Float4 Util::beige  = Float4(1,0.6,0.3,1);
Float4 Util::purple = Float4(1,0.5,1,1);
Float4 Util::lime   = Float4(0.5,1,0.5,1);

/////////////////////////////////////////////////////////
//
//    setMaterial();
//    DOES: sets  material properties to the given color
//
/////////////////////////////////////////////////////////

void Util::set_material(const Float4& color)
{
    float ambient = 0.1f;
    float diffuse = 0.6f;
    float specular = 0.4f;
    GLfloat mat[4];
    /**** set ambient lighting parameters ****/
    mat[0] = ambient*color.X();
    mat[1] = ambient*color.Y();
    mat[2] = ambient*color.Z();
    mat[3] = 1.0;
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT, mat);

    /**** set diffuse lighting parameters ******/
    mat[0] = diffuse*color.X();
    mat[1] = diffuse*color.Y();
    mat[2] = diffuse*color.Z();
    mat[3] = 1.0;
    glMaterialfv (GL_FRONT_AND_BACK, GL_DIFFUSE, mat);

    /**** set specular lighting parameters *****/
    mat[0] = specular;
    mat[1] = specular;
    mat[2] = specular;
    mat[3] = 1.0;
    glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, mat);
    glMaterialf (GL_FRONT_AND_BACK, GL_SHININESS, 8);
}

/////////////////////////////////////////////////////////////
//
// Draw a labeled arrow (used to show the coordinate axes)
//
// *** DO NOT CHANGE ****
/////////////////////////////////////////////////////////////

void Util::draw_arrow(const Point4 &p, const Vector4 &v, const Float4 &color,
                      double thickness, char *label)
{
    glEnable(GL_LIGHTING);
    set_material(color);
    glPushMatrix();

    // Go to middle of arrow.
    //
    Point4 midpoint = p + v/2;
    glTranslated(midpoint.X(),midpoint.Y(),midpoint.Z());
    //
    // Now rotate z-axis onto direction v.
    // Get rotation angle and axis.
    // Axis = z cross v = [-v.Y , v.X, 0]
    // cos(Angle) = (z dot v) / |v| = V.Z / |v|
    //
    //
    double lv = v.length();
    double angle = acos(v.Z()/lv) * 180.0 / 3.14159265;
    glRotated (angle, -v.Y(), v.X(), 0.0);
    glPushMatrix();
    glScaled (thickness, thickness, lv);
    draw_cube(1.0);
    glPopMatrix();
    //
    // Go to end of arrow, draw head.
    //
    glTranslated(0,0,lv/2);
    draw_cone(thickness*2, thickness*10, 8);

/*
    int len;
    if (label != NULL && (len=(int)strlen(label)) > 0)
    {
        glDisable(GL_LIGHTING);
        glColor3f(color.X(), color.Y(), color.Z());

        glRasterPos3d(0.3, 0.3, 0.3);
        int len = (int)strlen(label);
        for (int j=0; j<len; j++)
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, label[j]);
        glEnable(GL_LIGHTING);
    }
*/
    glPopMatrix();
}

/////////////////////////////////////////////////////////
//
// Draw the three basis vectors
//
// *** DO NOT CHANGE ***
/////////////////////////////////////////////////////////

void Util::draw_basis(float length)
{
    char X_label[] = {'X', '\0'};
    char Y_label[] = {'Y', '\0'};
    char Z_label[] = {'Z', '\0'};
    draw_arrow(Point4(0,0,0), Vector4(length,0,0), red,
               0.05, X_label);
    draw_arrow(Point4(0,0,0), Vector4(0,length,0), green,
               0.05, Y_label);
    draw_arrow(Point4(0,0,0), Vector4(0,0,length), blue,
               0.05, Z_label);
}

/////////////////////////////////////////
//
//    PROC: initLights()
//    DOES: Initializes lighting, and z-buffer test
//
/////////////////////////////////////////

void Util::init_lights(void)
{
    GLfloat ambient[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat position[] = { 3.0, 3.0, 3.0, 0.0 };

    GLfloat lmodel_ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat local_view[] = { 0.0 };

    /**** set lighting parameters ****/
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);

/*    glFrontFace (GL_CW); */
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void Util::draw_cube(double side) {
    double raw_vertices[8][3] = {{ 1, 1, 1},{-1, 1, 1},{ 1, 1,-1},{-1, 1,-1},
                                 { 1,-1, 1},{-1,-1, 1},{ 1,-1,-1},{-1,-1,-1}};
    double vertices[8][3];
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 3; j++)
            vertices[i][j] = raw_vertices[i][j] * side / 2;

    glBegin(GL_QUADS);
    glNormal3d(-1,0,0);
    glVertex3dv(vertices[0]);
    glVertex3dv(vertices[2]);
    glVertex3dv(vertices[6]);
    glVertex3dv(vertices[4]);

    glNormal3d(1,0,0);
    glVertex3dv(vertices[1]);
    glVertex3dv(vertices[3]);
    glVertex3dv(vertices[7]);
    glVertex3dv(vertices[5]);

    glNormal3d(0,-1,0);
    glVertex3dv(vertices[0]);
    glVertex3dv(vertices[1]);
    glVertex3dv(vertices[3]);
    glVertex3dv(vertices[2]);

    glNormal3d(0,1,0);
    glVertex3dv(vertices[4]);
    glVertex3dv(vertices[5]);
    glVertex3dv(vertices[7]);
    glVertex3dv(vertices[6]);

    glNormal3d(0,0,-1);
    glVertex3dv(vertices[2]);
    glVertex3dv(vertices[3]);
    glVertex3dv(vertices[7]);
    glVertex3dv(vertices[6]);

    glNormal3d(0,0,1);
    glVertex3dv(vertices[0]);
    glVertex3dv(vertices[1]);
    glVertex3dv(vertices[5]);
    glVertex3dv(vertices[4]);

    glEnd();
}

void Util::draw_sphere(double r, int lats, int longs) {
    int i, j;
    for(i = 0; i <= lats; i++) {
        double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
        double z0  = r*sin(lat0);
        double zr0 =  r*cos(lat0);

        double lat1 = M_PI * (-0.5 + (double) i / lats);
        double z1 = r*sin(lat1);
        double zr1 = r*cos(lat1);

        glBegin(GL_QUAD_STRIP);
        for(j = 0; j <= longs; j++) {
            double lng = 2 * M_PI * (double) (j - 1) / longs;
            double x = cos(lng);
            double y = sin(lng);

            glNormal3f(x * zr0, y * zr0, z0);
            glVertex3f(x * zr0, y * zr0, z0);
            glNormal3f(x * zr1, y * zr1, z1);
            glVertex3f(x * zr1, y * zr1, z1);
        }
        glEnd();
    }
}

void Util::draw_cone(double r, double h, int n_triangs) {
    // The pointy top
    glBegin(GL_TRIANGLE_FAN);
    glNormal3d(0,0,1);
    glVertex3d(0,0,h);
    double d_theta = M_PI * 2.0 / n_triangs;
    for(int i = 0; i <= n_triangs; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        glNormal3d(c, s, r/h);
        glVertex3d(r*c, r*s, 0);
    }
    glEnd();

    // The base
    glBegin(GL_TRIANGLE_FAN);
    glNormal3d(0,0,-1);
    glVertex3d(0,0,0);
    for(int i = 0; i <= n_triangs; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        glVertex3d(r*c, r*s, 0);
    }
    glEnd();
}

void Util::draw_cylinder(double r, double h, int n_staves) {
    // The sides
    glBegin(GL_QUAD_STRIP);
    double d_theta = M_PI * 2.0 / n_staves;
    for(int i = 0; i <= n_staves; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        glNormal3d(c, s, 0);
        glVertex3d(r*c, r*s, -h/2);
        glVertex3d(r*c, r*s,  h/2);
    }
    glEnd();

    // The bottom
    glBegin(GL_TRIANGLE_FAN);
    glNormal3d(0,0,-1);
    glVertex3d(0,0,-h/2);
    for(int i = 0; i <= n_staves; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        glVertex3d(r*c, r*s, -h/2);
    }
    glEnd();

    // The top
    glBegin(GL_TRIANGLE_FAN);
    glNormal3d(0,0, 1);
    glVertex3d(0,0,h/2);
    for(int i = 0; i <= n_staves; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        glVertex3d(r*c, r*s, h/2);
    }
    glEnd();
}

void Util::draw_torus(double r1, double r2, int n_lat, int n_lon) {
    Point4 *verts = new Point4[n_lat];
    Vector4 *normals = new Vector4[n_lat];
    double d_theta = M_PI * 2 / n_lat;
    double d_phi =  360 / n_lon;

    for (int i = 0; i < n_lat; i++) {
        double theta = d_theta * i;
        double c = cos(theta);
        double s = sin(theta);
        verts[i].set(r1 * c + r2, r1*s, 0);
        normals[i].set(c, s, 0);
    }

    Matrix4 turn = Matrix4::YRotation(d_phi);

    for (int i = 0; i < n_lat; i++) {
        int k = (i + 1) % n_lat;
        glBegin(GL_QUAD_STRIP);
        Point4 a = verts[i];
        Point4 b = verts[k];
        Vector4 Na = normals[i];
        Vector4 Nb = normals[k];
        for (int j = 0; j <= n_lon; j++) {
            glNormal3d(Na.X(), Na.Y(), Na.Z());
            glVertex3d(a.X(), a.Y(), a.Z());
            glNormal3d(Nb.X(), Nb.Y(), Nb.Z());
            glVertex3d(b.X(), b.Y(), b.Z());
            a = turn * a;
            b = turn * b;
            Na = turn * Na;
            Nb = turn * Nb;
        }
        glEnd();
    }

    delete[] verts;
    delete[] normals;
}
