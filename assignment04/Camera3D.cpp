#include "Camera3D.h"
#include <glad/glad.h>

////////////////////////////////////////////////////////////////////////
// Code for placing an OpenGL 3D camera using GLFW windowing
//
// The camera has these parameters:
// eye:    the center of projection
// ref:    where the eye is looking at
// up:     upward vector
//
// clipL,R,B,T,N,F : the six sides of the clipping frustum
//
// BL:     bottom-left of viewport
// width,height : dimensions of viewport
////////////////////////////////////////////////////////////////////////

Camera3D::Camera3D() {
    // Default camera constructor.
    _eye = Point4(5,5,5);
    _ref = Point4(0,0,0);
    _vup = Vector4(0,1,0);

    _clipL = -1;
    _clipR = +1;
    _clipB = -1;
    _clipT = +1;
    _clipN = 2;
    _clipF = 100;

    _BLx = 0;
    _BLy = 0;
    _width = 400;
    _height = 400;
}

Camera3D::Camera3D(GLFWwindow *window,
                   const Point4& eye, const Point4& lookat, const Vector4& vup,
                   float clipL, float clipR, float clipB, float clipT,
                   float clipN, float clipF,
                   int BLx, int BLy, int w, int h) {
    // Explicit constructor.  Stores viewport params, and
    // calls update to save other params.
    _window = window;
    _BLx    = BLx;
    _BLy    = BLy;
    _width  = w;
    _height = h;

    update(eye,lookat,vup,
           clipL,clipR,clipB,clipT,clipN,clipF);
}

void Camera3D::update(const Point4& eye,
                      const Point4& lookat,
                      const Vector4& vup,
                      float clipL, float clipR, float clipB, float clipT,
                      float clipN, float clipF) {
    // Saves 3D params of the camera
    _eye    = eye;
    _ref    = lookat;
    _vup    = vup;
    _clipL  = clipL;
    _clipR  = clipR;
    _clipB  = clipB;
    _clipT  = clipT;
    _clipN  = clipN;
    _clipF  = clipF;
}

void Camera3D::check_resize() {
    // If GLFW says window has changed shape, update viewport dimensions
    int width, height;

    glfwGetFramebufferSize(_window, &width, &height);
    _width  = width;
    _height = height;
}

void Camera3D::begin_drawing() {
    // Set up viewport transformation
    glViewport(_BLx, _BLy, _width, _height);

    // Set up clipping matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(_clipL, _clipR, _clipB, _clipT, _clipN, _clipF);

    // Initialize the model-view matrix.

    Vector4 x,y,z;         // the camera basis vectors
    z = (_eye - _ref).normalized();
    x = (_vup ^ z).normalized();
    y = z ^ x;

    // cout << "x:" << x << "\n";
    // cout << "y:" << y << "\n";
    // cout << "z:" << z << "\n";

    Matrix4 Rot(x[0], x[1], x[2], 0.0,
                y[0], y[1], y[2], 0.0,
                z[0], z[1], z[2], 0.0,
                0.0,   0.0,  0.0, 1.0);
    Matrix4 Trans = Matrix4::Translation(-_eye[0], -_eye[1], -_eye[2]);

    // cout << "Rot:\n";
    // cout << Rot;
    // cout << "Trans:\n";
    // cout << Trans;

    Matrix4 Mview = Rot * Trans;

    GLdouble mat[16];
    int i = 0;
    for (int row = 0; row < 4; row++)
        for (int col = 0; col < 4; col++)
            mat[i++] = Mview[col][row];

    // cout << "Mview:\n";
    // cout << Mview;

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(mat);
}
