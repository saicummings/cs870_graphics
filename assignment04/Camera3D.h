#ifndef __CAMERA3D_H__
#define __CAMERA3D_H__

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "GeomLib.h"

//-----------------------------------------------------------------------
// This class defines a 3D camera, which uses OpenGL to define a
// rectangular window for drawing.  It uses GLFW to handle window events
// Author: Alejo Hausner
// Date: Sept. 2017
//-----------------------------------------------------------------------

class Camera3D {
private:
    // Params defining View Coord system
    Point4 _eye;
    Point4 _ref;
    Vector4 _vup;

    // Params defining View Volume
    float _clipL, _clipR, _clipB, _clipT, _clipN, _clipF;

    // Params defining Viewport
    int _BLx, _BLy, _width, _height;

    // GLFW handle for current drawing window
    GLFWwindow* _window;

public:
    // Default constructor
    Camera3D();

    // Explicit constructor
    Camera3D(GLFWwindow *window,
             const Point4& eye, const Point4& lookat, const Vector4& vup,
             float clipL, float clipR, float clipB, float clipT,
             float clipN, float clipF,
             int BLx, int BLy, int w, int h);

    void update(const Point4& eye,
                const Point4& lookat,
                const Vector4& vup,
                float clipL, float clipR, float clipB, float clipT,
                float clipN, float clipF);

    // Call this to update the camera, in case the
    // window has been resized;
    void check_resize();

    // Call this before you start any drawing commands
    void begin_drawing();
};

#endif
