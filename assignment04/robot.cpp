////////////////////////////////////////////////////
//
// NAME: Sai Lekyang
//
// robot.cpp:
// Code for drawing an articulated creature.
////////////////////////////////////////////////////

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "GeomLib.h"
#include "KBUI.h"
#include "Camera3D.h"
#include "Util.h"

KBUI the_ui;
Camera3D camera;
Camera3D original_camera;

void reset_angles(float);
void reset_camera(float);
void camera_changed(float);

///////////////////////////////////////////
// These are the variables that the user controls.
//
// ***** NOTE!!!! ***********
//
// When the angles are all zero, the robot
// should be standing upright, with arms straight
// down by its sides, and legs straight,
// looking straight ahead.
//
//////////////////////////////////////////

float rightShoulderAngle;   // angle under the right armpit
float leftShoulderAngle;    // angle under the left armpit
float rightElbowAngle;      // angle at the right elbow
float leftElbowAngle;       // angle at the left elbow
float headTurnAngle;        // left-right head angle
float headTiltAngle;        // forward-back head angle
float elevationAngle;       // body tilt angle above base (latitude)
float azimuthAngle;         // body rotation angle around base (longitude)
float rollAngle;            // body rotation around its up-down axis

// Check if these are 0 or 1, treating them as flags for
// displaying the axes at crucial joints

float showBodyAxes = 1;
float showHeadAxis = 1;
float showRightArmAxes = 1;
float showLeftArmAxes = 1;

// These are the camera parameters.
Point4  eye(2, 4, 10);
Point4  lookat(0, 4, 0);
Vector4 vup(0, 1, 0);

float leftX  = -1;
float rightX = +1;
float bottomY= -1;
float topY   = +1;
float nearZ  = 1.5;
float farZ   = 20;

// The light's position (in view coords)
GLfloat lightPos[4] = {-2.0, 2.0, 5.0, 1.0};

void init_UI() {
    the_ui.add_variable("Show Body axes", &showBodyAxes,    0,   1,  1);
    the_ui.add_variable("Show Head axis", &showHeadAxis,    0,   1,  1);
    the_ui.add_variable("Head Left/Right",&headTurnAngle, -90,  90,  5);
    the_ui.add_variable("Head Up/Down",   &headTiltAngle, -45,  45,  5);

    the_ui.add_variable("Show Left Arm Axes", &showLeftArmAxes,   0,   1, 1);
    the_ui.add_variable("Left Shoulder", &leftShoulderAngle,      0, 135, 5);
    the_ui.add_variable("Left Elbow",    &leftElbowAngle,         0, 135, 5);

    the_ui.add_variable("Show Right Arm Axes", &showRightArmAxes, 0,   1, 1);
    the_ui.add_variable("Right Shoulder", &rightShoulderAngle,    0, 135, 5);
    the_ui.add_variable("Right Elbow",    &rightElbowAngle,       0, 135, 5);

    the_ui.add_variable("Body Azimuth",   &azimuthAngle,   -180, 180, 10);
    the_ui.add_variable("Body Elevation", &elevationAngle,  -90,  90, 10);
    the_ui.add_variable("Body Roll",      &rollAngle,      -180, 180, 10);

    // Unlike all the above variables, these ones will trigger
    // a call-back when they are changed.
    the_ui.add_variable("Eye X", &eye.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Eye Y", &eye.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Eye Z", &eye.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Ref X", &lookat.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Ref Y", &lookat.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Ref Z", &lookat.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Vup X", &vup.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Vup Y", &vup.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Vup Z", &vup.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Clip L", &leftX,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip R", &rightX,  -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip B", &bottomY, -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip T", &topY,    -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip N", &nearZ,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip F", &farZ,    -10, 10, 0.2, camera_changed);

    float dummy1=0;
    float dummy2=0;
    the_ui.add_variable("Reset Angles", &dummy1,0,100, 0.001, reset_angles);
    the_ui.add_variable("Reset Camera", &dummy2,0,100, 0.001, reset_camera);

    the_ui.done_init();

}

/////////////////////////////////////////////////////////
//
// Code to draw the figure, using all the degrees of
// freedom supplied by the user.
//
// ********************** YOU MUST CHANGE THIS!!!! ******
//
/////////////////////////////////////////////////////////
void drawFigure() {
    if (showBodyAxes)
        Util::draw_basis(1);
    glTranslated(0, 0, 0);
    glPushMatrix();
    // base
    {
        Util::set_material(Util::beige);
        glScaled(2, 1, 2);
        Util::draw_cube(1);
    }
    glPopMatrix();

    // azimuth/elevation pivot ball
    glTranslated(0, 1.5, 0);
    glRotated(azimuthAngle, 0,1,0);
    glRotated(elevationAngle, 1,0,0);
    if (showBodyAxes)
        Util::draw_basis(1);
    Util::set_material(Util::purple);
    Util::draw_sphere(1, 12,12);

    // roll cylinder
    glTranslated(0, 1.5, 0);
    glRotated(rollAngle, 0,1,0);
    if (showBodyAxes)
        Util::draw_basis(1);

    //+
    glPushMatrix();
        glRotated(90, 1, 0, 0);
        Util::set_material(Util::green);
        Util::draw_cylinder(1,1,16);
    glPopMatrix();

    glTranslated(0, 2.5, 0);

    // torso
    if (showBodyAxes)
        Util::draw_basis(1);
    glPushMatrix();
    { // curly braces help demarcate push/pop pairs
        glScaled(2,4,1);
        Util::set_material(Util::beige);
        Util::draw_cube(1);
    }
    glPopMatrix();

    glPushMatrix(); // remember body center.
    {
        // draw left shoulder pivot
        glTranslated(1.5,2,0);
        glRotated(leftShoulderAngle, 0,0,1);
        if (showLeftArmAxes)
            Util::draw_basis(1);

        //+    
        Util::set_material(Util::green);
        Util::draw_cylinder(0.5,1,16);


        // upper left arm
        glTranslated(0, -1.5, 0);
        glPushMatrix();
        {
            glScaled(1, 2, 1);
            Util::set_material(Util::beige);
            Util::draw_cube(1);
        }
        glPopMatrix();

        glPushMatrix();
        //+
            glTranslated(0, -1.5, 0);
            glRotated(leftElbowAngle, 0,0,1);
            if (showLeftArmAxes)
                Util::draw_basis(1);
            Util::set_material(Util::green);
            Util::draw_cylinder(0.5,1,16);
            glTranslated(0, -1.5, 0);            
            glScaled(1, 2, 1);
            Util::set_material(Util::beige);
            Util::draw_cube(1);
        
        glPopMatrix();
    }
    glPopMatrix();  // back at body center

    glPushMatrix(); // remember body center.
        glTranslated(0,2.75,0);
        glRotated(headTurnAngle, 0,1,0);
        glRotated(headTiltAngle, 1,0,0);
        if (showHeadAxis)
                Util::draw_basis(1);
        Util::set_material(Util::beige);
        Util::draw_cylinder(0.75,1,16);
        
        // Eyes
        glPushMatrix(); 
            Util::set_material(Util::red);
            glTranslated(-0.3,0.3,0.4);
            Util::draw_sphere(0.2, 12,12);
        glPopMatrix();
        glPushMatrix();
            Util::set_material(Util::green);
            glTranslated(0.3,0.3,0.4);
            Util::draw_sphere(0.2, 12,12);
        glPopMatrix();
    glPopMatrix();  // back at body center


    glPushMatrix(); // remember body center.
    {
        // draw right shoulder pivot
        glTranslated(-1.5,2,0);
        glRotated(-rightShoulderAngle, 0,0,1);
        if (showRightArmAxes)
            Util::draw_basis(1);

        //+    
        Util::set_material(Util::green);
        Util::draw_cylinder(0.5,1,16);


        // upper right arm
        glTranslated(0, -1.5, 0);
        glPushMatrix();
        {
            glScaled(1, 2, 1);
            Util::set_material(Util::beige);
            Util::draw_cube(1);
        }
        glPopMatrix();

        glPushMatrix();
        //+
            glTranslated(0, -1.5, 0);
            glRotated(-rightElbowAngle, 0,0,1);
            if (showRightArmAxes)
                Util::draw_basis(1);
            Util::set_material(Util::green);
            Util::draw_cylinder(0.5,1,16);
            glTranslated(0, -1.5, 0);            
            glScaled(1, 2, 1);
            Util::set_material(Util::beige);
            Util::draw_cube(1);
        
        glPopMatrix();
    }
    glPopMatrix();
}

//////////////////////////////////////////////////////
//
//    This function gets called for any keypresses
//
//  **** DO NOT CHANGE ****
//////////////////////////////////////////////////////

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if (key == GLFW_KEY_Q ||
        key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    else if (action == GLFW_RELEASE) {
        the_ui.handle_key(key);
    }
}

void camera_changed(float dummy) {
    camera.update(eye, lookat, vup,
                  leftX, rightX, bottomY, topY, nearZ, farZ);
};

//////////////////////////////////////////////////////////////////////
//
//   This gets called by the main program to draw the scene.
//
// *** YOU PROBABLY DON'T NEED TO CHANGE THIS ****
//
//////////////////////////////////////////////////////////////////////

void display(void) {
    //
    // Set the background color to grey-blue.
    //
    glClearColor(.4f,.4f, 0.7f,1.f);

    //
    // OK, now clear the screen with the background color
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //
    // Position the camera eye and look-at point.
    //
    camera.begin_drawing();

    // Place the light source.
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);


    drawFigure();

    //
    // Execute any GL functions that are in the queue.
    //
    glFlush();
}

//////////////////////////////////////////////////////////////////////
//
// Set all camera params to their default values.
//
//////////////////////////////////////////////////////////////////////

void reset_camera(float dummy) {
    camera = original_camera;
}

//////////////////////////////////////////////////////////////////////
//
// Set the figure's angles to their default values.
//
//////////////////////////////////////////////////////////////////////

void reset_angles(float dummy) {
    rightShoulderAngle = 0;
    leftShoulderAngle = 0;
    rightElbowAngle = 0;
    leftElbowAngle = 0;
    headTurnAngle = 0;
    headTiltAngle = 0;
    azimuthAngle = 0;
    elevationAngle = 0;
    rollAngle = 0;
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

/*********************************************************
     PROC: main()
     DOES: calls initialization, then enters control loop.
**********************************************************/

int main(int argc, char** argv)
{
    GLFWwindow* window;
    int w = 600;
    int h = 600;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    init_UI();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "Draw a Robot", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS <ENTER>\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    camera = Camera3D(window, eye, lookat, vup,
                      leftX, rightX, bottomY, topY, nearZ, farZ,
                      0, 0, w, h);
    original_camera = camera;

    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    Util::init_lights();

    while (!glfwWindowShouldClose(window))
    {
        camera.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
