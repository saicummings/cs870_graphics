// Saisuk Lekyang Assignment 3 resubmit

#include <iostream>

typedef unsigned char pixel;
using namespace std;

void turn_image(pixel **image, int n) {
    // image: a 2D array of bytes.
    // To access the pixel at row y, column x, use image[y][x].

    // transpose 
    for(int j = 0; j < n; j++){	
        for(int i = j; i < n; i++){
            char t = image[i][j];
                image[i][j] = image[j][i];
                image[j][i] = t;
        }

    }

    // flip 
    for(int j = 0; j < n; j++){
        int width = n;
        for(int i = 0; i < n / 2; i++){
            char t = image[i][j];
            image[i][j] = image[--width][j];
            image[width][j] = t;
        }
    } 


}

void print_image(pixel **image, int n) {
    for (int y=0; y < n; y++) {
        for (int x=0; x < n; x++)
            cout << image[y][x] << " ";
        cout << "\n";
    }
}

int main(int argc, char *argv[]) {
    // MAIN PROGRAM: Do not change.
    pixel **image;
    int n = 7;
    image = new pixel*[n];
    for (int y=0; y < n; y++) {
        image[y] = new pixel[n];
        for (int x=0; x < n; x++)
            image[y][x] = '.';
    }
    image[0][0] = image[0][6] = '*';
    image[1][1] = image[1][6] = '*';
    image[2][1] = image[2][6] = '*';
    image[3][1] = image[3][6] = '*';
    image[4][1] = image[4][5] = '*';
    image[5][2] = image[5][4] = '*';
    image[6][3] = '*';

    cout << "BEFORE ROTATION:\n";
    print_image(image, n);

    turn_image(image, n);
    cout << "AFTER ROTATION:\n";
    print_image(image, n);
}


