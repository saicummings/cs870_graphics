//========================================================================
// Draw a robot-like shape
// Author: Alejo Hausner
// Date: Oct. 2018.
// Addition: Saisuk Lekyang
//========================================================================

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>

#include "camera.h"

using namespace std;

Camera cam;

//////////////////////////////////////////////////////////////////////
// Draw a 1x1 box, with bottom-left corner at (0 0)
//////////////////////////////////////////////////////////////////////

void square() {
    glBegin(GL_POLYGON);
    glVertex2d(0,0);
    glVertex2d(0,1);
    glVertex2d(1,1);
    glVertex2d(1,0);
    glEnd();
}

//////////////////////////////////////////////////////////////////////
// Draw a branching figure that looks like a robot
//////////////////////////////////////////////////////////////////////
void draw_robot() {
    glLoadIdentity();

    glPushMatrix();
        glScaled(1, 2, 1);
        square();
    glPopMatrix();

    glTranslated(0, 2, 0);
    
    glPushMatrix();
        glRotated(45, 0, 0 ,1);
        glTranslated(-1, 0, 0);

        glPushMatrix();
            glScaled(1, 2, 1);
            square();
        glPopMatrix();

        glTranslated(0, 2, 0);
        glRotated(135, 0, 0 ,1);
  
        glPushMatrix();
            glScaled(0.5, 1, 1);
            square();
        glPopMatrix();
    glPopMatrix();

    glPushMatrix();
        
        glTranslated(1, 0, 0);
        glRotated(-135, 0, 0 ,1);
        glTranslated(-1, 0, 0);
        glPushMatrix();
            glScaled(1, 2, 1);
            square();
        glPopMatrix();

        
        glTranslated(0, 2, 0);
        glRotated(45, 0, 0 ,1);
  
        glPushMatrix();
            glScaled(0.5, 1, 1);
            square();
        glPopMatrix();

    
    glPopMatrix();



}

//////////////////////////////////////////////////////////////////////
//
// Call this to redraw the scene.
//
//////////////////////////////////////////////////////////////////////

void display(void)
{
    //
    // Set the background colour to darkish blue-gray.
    //
    glClearColor(.4f,.4f, 0.6f,1.f);

    //
    // OK, now clear the screen with the background colour
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    //
    // draw the coordinate grid
    //
    cam.draw_grid(0.1, 1);

    draw_robot();
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_Q ) &&
        action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

int main(int argc, char *argv[])
{
    GLFWwindow* window;
    int w = 900;
    int h = 600;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(w, h, "Branching Figure", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    cam = Camera(-3,-1, 9, 6, w, h, window);

    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
