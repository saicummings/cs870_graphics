#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "GeomLib.h"
#include "Color.h"

class Light {
public:
    Point4 position;
    Color color;
    Color ambient_color;

    Light();
	Light(Color c, Point4 p);
};

#endif
