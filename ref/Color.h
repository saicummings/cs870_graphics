#if !defined(_COLOR_H_)

#define _COLOR_H_

#include "GeomLib.h"

class Color : public Float4 {

public:
    float R();
    float G();
    float B();
	float red, green, blue;

	void set(float r, float g, float b);

	Color operator+(const Color& other);

    Color(float r, float g, float b);
	Color();
};

#endif

