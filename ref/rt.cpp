//////////////////////////////////////////////////////
//
// A simple ray tracer.
//
//////////////////////////////////////////////////////

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

#include "Camera.h"
#include "KBUI.h"

#include "GeomLib.h"
#include "Color.h"
#include "Material.h"
#include "Object.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Light.h"
#include "Hit.h"

using namespace std;

KBUI the_ui;
Camera cam;

void camera_changed(float);
void reset_camera(float);
void reRender();

ofstream dbgfile("debug.dat");
bool debugOn = false;
typedef unsigned char byte;

// The initial image is 100 x 100 x 3 bytes (3 bytes per pixel)
int winWidth  = 100;
int winHeight = 100;
byte *img = NULL;

// These are the camera parameters.
// The camera position and orientation:
Point4  eye(0, 0, 4);
Point4  lookat(0, 0, 0);
Vector4 vup(0, 1, 0);

// The clipping frustum
float clipL = -1;
float clipR = +1;
float clipB = -1;
float clipT = +1;
float clipN =  2;

vector<Object*> sceneObjects; // list of object in the scene

vector<Light> sceneLights; // list of lights in the scene
Color sumLight; 

vector<Material> materials; // list of available materials

Matrix4 Mvcswcs;  // the inverse of the view matrix.
Hit *hitPool;     // array of available hit records.

Color ambientLight; // indirect light that shines when all lights are blocked

bool frame_buffer_stale = true;
int mouse_x, mouse_y;

// Forward declarations for functions in this file
void init_UI();
void setupCamera();
void setRay(int xDCS, int yDCS, Ray4& ray);
Vector4 mirrorDirection(Vector4& L, Vector4& N);
Color localIllum(Vector4& V, Vector4& N, Vector4& L,
                 Material& mat, Color& ls);
Color glossy_color(Ray4 &ray, Hit hit);
Hit firstHit(Ray4 &ray);
void camera_changed(float dummy);
void reRender();
Color rayColor(int xDCS, int yDCS);
void render();
string downcase(const string &s);
void match(ifstream &file, const string& pattern);
void readScene(char *sceneFile);
void reset_camera(float dummy);
void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods );
void mouse_position_callback( GLFWwindow* window, double x, double y );
static void error_callback(int error, const char* description);
void display ();
static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods);
int main(int argc, char *argv[]);


///////////////////////////////////////////////////////////////////////
// Initialize the keyboard-driven UI.
// (no need to change this)
///////////////////////////////////////////////////////////////////////
void init_UI() {
    // These variables will trigger a call-back when they are changed.
    the_ui.add_variable("Eye X", &eye.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Eye Y", &eye.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Eye Z", &eye.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Ref X", &lookat.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Ref Y", &lookat.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Ref Z", &lookat.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Vup X", &vup.X(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Vup Y", &vup.Y(), -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Vup Z", &vup.Z(), -10, 10, 0.2, camera_changed);

    the_ui.add_variable("Clip L", &clipL,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip R", &clipR,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip B", &clipB,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip T", &clipT,   -10, 10, 0.2, camera_changed);
    the_ui.add_variable("Clip N", &clipN,   -10, 10, 0.2, camera_changed);

    float dummy2;
    the_ui.add_variable("Reset Camera", &dummy2,0,100, 0.001, reset_camera);

    the_ui.done_init();

}

/////////////////////////////////////////////////////////////////////////
// Initialize the VCS-to-WCS matrix
// (You should implement this function)
/////////////////////////////////////////////////////////////////////////
void setupCamera() {

	// The camera's basis vectors
	Vector4 cam_Z = (eye - lookat).normalized();
	Vector4 cam_X = (vup ^ cam_Z).normalized();
	Vector4 cam_Y = cam_Z ^ cam_X;

	// The camera-to-world matrix
	Mvcswcs = Matrix4(cam_X.X(), cam_Y.X(), cam_Z.X(), eye.X(),
		cam_X.Y(), cam_Y.Y(), cam_Z.Y(), eye.Y(),
		cam_X.Z(), cam_Y.Z(), cam_Z.Z(), eye.Z(),
		0,         0,         0,         1);

}

/////////////////////////////////////////////////////////////////////////
// Create a ray which starts at the given (x y)DCS pixel
// (You should implement this function)
/////////////////////////////////////////////////////////////////////////

void setRay(int xDCS, int yDCS, Ray4& ray) {

	// The pixel in DCS
	Point4 P_dcs(xDCS, yDCS, 0);

	// The pixel in VCS
	float dx = (clipR - clipL) / winWidth;
	float dy = (clipT - clipB) / winHeight;
	float x_vcs = clipL + (P_dcs.X() + 0.5) * dx;
	float y_vcs = clipB + (P_dcs.Y() + 0.5) * dy;
	float z_vcs = -clipN;
	Point4 P_vcs(x_vcs, y_vcs, z_vcs);


	setupCamera();

	// Pixel in WCS
	Float4 F_wcs = Mvcswcs * P_vcs;
	Point4 P_wcs(F_wcs.X(), F_wcs.Y(), F_wcs.Z());


	ray = Ray4(P_wcs, (P_wcs - eye).normalized());

}

/////////////////////////////////////////////////////////////////////////
// Given a light direction and surface normal, get the
// mirror direction (No need to change this)
/////////////////////////////////////////////////////////////////////////
Vector4 mirrorDirection(Vector4& L, Vector4& N) {
    float NL = N * L;
    return (2.0f * NL) * N - L;
}

/////////////////////////////////////////////////////////////////////////
// Compute the Phong local illumination color.
// (You should implement this function)
/////////////////////////////////////////////////////////////////////////
Color localIllum(Vector4& V, Vector4& N, Vector4& L,
                 Material& mat, Color& ls) {
	//L = L.normalized();
	Vector4 R = mirrorDirection(L, N);
	Color ka = mat.getAmbient(), kd = mat.getDiffuse(), ks = mat.getSpecular();
	//cout << sumLight.R() << endl;
	Color Ia = Color(0.3 * sumLight.R(), 0.3 * sumLight.G(), 0.3 * sumLight.B());

	float v = (R * V);

	int n = mat.getShininess();
	//if(n>0)
	//cout << n << endl;
	Color Ip = Color(Ia.R() * ka.R() + ls.R() * (kd.R() * N * L + ks.R() * pow(v, n)),
		Ia.G() * ka.G() + ls.G() * (kd.G() * N * L + ks.G() * pow(v, n)), 
		Ia.B() * ka.B() + ls.B() * (kd.B() * N * L + ks.B() * pow(v, n)));

	//cout << Ip.R() << endl;

    return Ip;
}

/////////////////////////////////////////////////////////////////////////
// Find the first object hit by the ray, if any
// (You should implement this function)
/////////////////////////////////////////////////////////////////////////

Hit firstHit(Ray4 &ray) {

	hitPool = new Hit[10];
	int ind = 0;
	Hit minHit;

	
	for each (Object *obj in sceneObjects)
	{
		//cout << "chutoya" << endl;
		Hit h;
		if (obj->intersects(ray, h)) {
			hitPool[ind] = h;
			ind++;
			if (h.getTValue() < minHit.getTValue())
				minHit = h;
			
		}	
	}
    return minHit;
}

/////////////////////////////////////////////////////////////////////////
// Triggered by KBUI when user changes a camera parameter
// (No need to change this)
/////////////////////////////////////////////////////////////////////////
void camera_changed(float dummy) {
    reRender();
}

/////////////////////////////////////////////////////////////////////////
// Called when picture needs to be rendered again.
// (No need to change this)
/////////////////////////////////////////////////////////////////////////
void reRender() {
    frame_buffer_stale = true;
}


/////////////////////////////////////////////////////////////////////////
// Create a ray starting at (x y)DCS,
// test it against all objects in the scene,
// get the first object hit,
// and compute the glossy color at the hit point,
//
// (You must implement this function)
/////////////////////////////////////////////////////////////////////////
Color rayColor(int xDCS, int yDCS) {

	Ray4 r;
	setRay(xDCS, yDCS, r);
	
	Hit h = firstHit(r);
	//if(h.material.getShininess()>0)
	//cout << h.material.getShininess()<<endl;

	return glossy_color(r, h);
}

Color glossy_color(Ray4 &ray, Hit hit) {


	Color c(0.0, 0.0, 0.0);
	//if(hit.material.getShininess() > 0)
	
	for each(Light l in sceneLights) {
		
		//cout << hit.material.getShininess() << endl;
		if(hit.material.getShininess() > 0)
		c = c + localIllum(-ray.direction, hit.normal, (l.position - hit.hit_point).normalized(), hit.material, l.color);/* +
			Color(hit.material.getAmbient().R() * l.ambient_color.R(),
			hit.material.getAmbient().G() * l.ambient_color.G(),
			hit.material.getAmbient().B() * l.ambient_color.B());	*/	
	}
	//if(c.R()) != 0)
	//cout << c << endl;
	
	return c;
}


/////////////////////////////////////////////////////////////////////////
//
// This function actually generates the ray-traced image.
// This is where your main code will go.
// Currently, it fills the current image with black,
//   except for a white grid of horizontal and vertical
//   lines spaced 10 pixels apart.
// (You must implement this function)
//
/////////////////////////////////////////////////////////////////////////

void render() {
    int x,y;
    float r,g,b;
    int p;

    // The following code needs to be replaced with
    // An actual ray-tracer!  It just goes through
    // all the DCS coordinates, and paints a grid
    // on the frame buffer.

    //
    // Make all pixels black.
    //
	/*
	for (y = 0; y < winHeight; y++)
	{
		for (x = 0; x < winWidth; x++)
		{
			r = g = b = 0;
			p = (y*winWidth + x) * 3;

			img[p++] = r;
			img[p++] = g;
			img[p] = b;
		}
	}

	*/
    for (y=0; y<winHeight; y++)
    {
        for (x=0; x<winWidth; x++)
        {
			Color c = rayColor(x, y);
            r = c.R();
			g = c.G();
			b = c.B();
			if (r < 0)
				r = 0;
			if (g < 0)
				g = 0;
			if (b < 0)
				b = 0;
			if (r > 1)
				r = 1;
			if (g > 1)
				g = 1;
			if (b > 1)
				b = 1;

			//cout << r << endl;
            p = (y*winWidth + x) * 3;

			img[p++] = r * 255.0f;
			img[p++] = g * 255.0f;
			img[p] = b * 255.0f;
        }
    }
	/*

    //
    // Make every tenth pixel white in the y direction
    //
    for (y=0; y<winHeight; y++)
    {
        for (x=0; x<winWidth; x += 10)
        {
            r = g = b = 255;
            p = (y*winWidth + x) * 3;

            img[p++] = r;
            img[p++] = g;
            img[p] = b;
        }
    }

    //
    // And in the x direction.
    //
    for (x=0; x<winWidth; x ++)
    {
        for (y=0; y<winHeight; y+=10)
        {
            r = g = b = 255;
            p = (y*winWidth + x) * 3;

            img[p++] = r;
            img[p++] = g;
            img[p] = b;
        }
    }*/
}

/////////////////////////////////////////////////////////////////////////
// Turn string to lowercase.
// (No need to change this)
/////////////////////////////////////////////////////////////////////////

string downcase(const string &s) {
    string result = "";
    for (int i=0; i<(int)s.length(); i++) {
        char c = s[i];
        if ('A' <= c && c <= 'Z')
            c = c - 'A' + 'a';
        result += c;
    }
    return result;
}

/////////////////////////////////////////////////////////////////////////
// Convenience function for checking that the upcoming
// word in the file matches what you are expecting.
// (No need to change this)
/////////////////////////////////////////////////////////////////////////

void match(ifstream &file, const string& pattern) {
    string word;
    file >> word;
    word = downcase(word);
    if (word != string(pattern)) {
        cerr << "Parse error on data file: expected \""
             << pattern << "\" but found \"" << word << "\"\n";
        exit(EXIT_FAILURE);
    }
}

//////////////////////////////////////////////////////
//
// This function reads the scene from the data file,
// which is an argument given on the command line.
// (You must implement this function)
//
/////////////////////////////////////////////////////
void readScene(char *sceneFile) {
    ifstream file(sceneFile);
    if (!file) {
        cerr << "Can't read from " << sceneFile << endl;
        exit(EXIT_FAILURE);
    }
	
	float r, g, b;
	float x, y, z;

	while (!file.eof()) {
		string keyword;
		file >> keyword;
		keyword = downcase(keyword);

		if (keyword == string("")) {
			continue; // skip blank lines
		}
		else if (keyword == string("#materials")) {
			int nMaterials;
			file >> nMaterials;
			// Make the materials vector exactly the right size
			materials.reserve(nMaterials);
		}

		else if (keyword == string("material")) {
			match(file, "ambient");
			file >> r >> g >> b;
			Color ka(r, g, b);

			match(file, "diffuse");
			file >> r >> g >> b;
			Color kd(r, g, b);

			match(file, "specular");
			file >> r >> g >> b;
			Color ks(r, g, b);

			match(file, "shininess");
			int n;
			file >> n;

			materials.push_back(Material(ka, kd, ks, n));
		}
		else if (keyword == string("#lights")) {
			int nMaterials;
			file >> nMaterials;
			// Make the materials vector exactly the right size
			sceneLights.reserve(nMaterials);
		}
		else if (keyword == string("light")) {
			
			match(file, "color");
			file >> r >> g >> b;
			Color c(r, g, b);
			//cout << c.B() << endl;
			//cout <<"early "<< c.G() << endl;
			sumLight = sumLight + c;
			//sumLight.set(sumLight.R() + c.R(), sumLight.G() + c.G(), sumLight.B() + c.B());
			//cout << "later " <<sumLight.G() << endl;
			match(file, "position");
			file >> x >> y >> z;
			Point4 p(x, y, z);

			sceneLights.push_back(Light(c, p));
		}
		else if (keyword == string("#objects")) {
			int nMaterials;
			file >> nMaterials;
			// Make the materials vector exactly the right size
			sceneObjects.reserve(nMaterials);
			
		}
		else if (keyword == string("sphere")) {
			match(file, "center");
			file >> x >> y >> z;
			Point4 center(x, y, z);

			match(file, "radius");
			float radius;
			file >> radius;

			match(file, "material");
			int index;
			file >> index;
			//cout << materials.at(index).getShininess() << endl;

			sceneObjects.push_back(new Sphere(center, radius, materials.at(index)));
		}

		else if (keyword == string("triangle")) {
			match(file, "vertex");
			file >> x >> y >> z;
			Point4 v1(x, y, z);

			match(file, "vertex");
			file >> x >> y >> z;
			Point4 v2(x, y, z);

			match(file, "vertex");
			file >> x >> y >> z;
			Point4 v3(x, y, z);

			match(file, "material");
			int index;
			file >> index;

			sceneObjects.push_back(new Triangle(v1, v2, v3, materials.at(index)));
		}
		else if (keyword == "camera_eye") {

			file >> x >> y >> z;
			Point4 cE(x, y, z);
			eye = cE;
		}
		else if (keyword == "camera_lookat") {
			
			file >> x >> y >> z;
			Point4 cL(x, y, z);
			lookat = cL;
		}
		else if (keyword == "camera_vup") {
			
			file >> x >> y >> z;
			Vector4 cV(x, y, z);
			vup = cV;
		}
		else if (keyword == "camera_clip") {
			
			file >> clipL >> clipR >> clipB >> clipT >> clipN;
			
		}
		else {
			cout << "--------------------------------READING ERROR---------------------------------------------------";
		}

	}
}

///////////////////////////////////////////////////
//
// Resets the camera parameters.
//
// **** NO NEED TO CHANGE THIS ****
//
///////////////////////////////////////////////////

void reset_camera(float dummy) {
    eye.X() = 0;
    eye.Y() = 0;
    eye.Z() = 4;

    lookat.X() = 0;
    lookat.Y() = 0;
    lookat.Z() = 0;

    vup.X() = 0;
    vup.Y() = 5;
    vup.Z() = 0;

    clipL = -1;
    clipR = +1;
    clipB = -1;
    clipT = +1;
    clipN =  2;
}

//////////////////////////////////////////////////////
//
// Displays, on STDOUT, the color of the pixel that
//  the user clicked on.
//
// **** NO NEED TO CHANGE THIS ****
//
//////////////////////////////////////////////////////

void mouse_button_callback( GLFWwindow* window, int button,
                            int action, int mods )
{
    if (button != GLFW_MOUSE_BUTTON_LEFT)
        return;

    if (action == GLFW_PRESS)
    {
        double wx, wy;
        cam.mouse_to_world(mouse_x, mouse_y, wx, wy);

        debugOn = true;
        Color pixelColor = rayColor(wx, wy);
        debugOn = false;

        cout << "Pixel Color = " << pixelColor << endl;
    }
    else {
        // Here, record the fact that NOTHING is being pressed
        // on anymore.
    }
}

//////////////////////////////////////////////////////
// Gets called when the mouse moves.
// (No need to change this).
//////////////////////////////////////////////////////
void mouse_position_callback( GLFWwindow* window, double x, double y )
{
    // Here, if mouse is currently pressing on some vertex,
    // change the vertex's position.

    mouse_x = (int)x;
    mouse_y = (int)y;
    double wx, wy;
    cam.mouse_to_world(mouse_x, mouse_y, wx, wy);
}

//////////////////////////////////////////////////////
// Gets called if there was an error in GLFW.
// (No need to change this)
//////////////////////////////////////////////////////
static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

//////////////////////////////////////////////////////
//
// Show the image.
//
// **** NO NEED TO CHANGE THIS ****
//
//////////////////////////////////////////////////////

void display () {
    glClearColor(.1f,.1f,.1f, 1.f);   /* set the background colour */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam.begin_drawing();

    glRasterPos3d(0.0,0.0,0.0);

    glPixelStorei(GL_PACK_ALIGNMENT,1);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    if (frame_buffer_stale) {
        //
        // Don't re-render the scene EVERY time display() is called.
        // It might get called if the window is moved, or if it
        // is exposed.  Only re-render if the window is RESIZED.
        // Resizing is detected by the Camera class,
        // frameBufferStale.
        //
        render();
        frame_buffer_stale = false;
    }

    //
    // This paints the current image buffer onto the screen.
    //
    glDrawPixels(winWidth,winHeight,
                 GL_RGB,GL_UNSIGNED_BYTE,img);

    glFlush();
}

void window_resized(int w, int h)
{
    winWidth  = w;
    winHeight = h;

    //

    if (img != NULL)
        delete [] img;
    img = new byte[winWidth*winHeight*3];

    //
    // Ask for image to be re-drawn, eventually.
    //
    reRender();
}

//////////////////////////////////////////////////////
//
// Basically, quit if the user hits "q" or "ESC".
//
// **** NO NEED TO CHANGE THIS ****
//
//////////////////////////////////////////////////////

static void key_callback(GLFWwindow* window, int key,
                         int scancode, int action, int mods)
{
    if (key == GLFW_KEY_Q ||
        key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    else if (action == GLFW_RELEASE) {
        the_ui.handle_key(key);
    }
}


//////////////////////////////////////////////////////
//
// Main program.
//
// **** NO NEED TO CHANGE THIS ****
//
//////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage:\n";
        std::cerr << "  rt <scene_file.txt>\n";
        char line[100];
        std::cin >> line;
        exit(EXIT_FAILURE);
    }

    GLFWwindow* window;

    glfwSetErrorCallback(error_callback);

    if (!glfwInit()) {
        cerr << "glfwInit failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(winWidth, winHeight,
                              "Ray Traced Scene", NULL, NULL);

    if (!window)
    {
        cerr << "glfwCreateWindow failed!\n";
        cerr << "PRESS Control-C to quit\n";
        char line[100];
        cin >> line;

        glfwTerminate();
        exit(EXIT_FAILURE);
    }

	readScene(argv[1]);

    int w = winWidth;
    int h = winHeight;

    window_resized(w, h);

    cam = Camera(0,0, w,h, w, h, window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window,   mouse_position_callback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    while (!glfwWindowShouldClose(window))
    {
        cam.check_resize();

        if (cam.get_win_W() != winWidth ||
            cam.get_win_H() != winHeight) {

            window_resized(cam.get_win_W(), cam.get_win_H());
        }

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}

