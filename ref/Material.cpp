#include "Material.h"

Material::Material(Color& ambient, Color& diffuse, Color& specular, int shininess) {
	ka = ambient;
	kd = diffuse;
	ks = specular;
	n = shininess;
}

void Material::set(Color& ambient, Color& diffuse, Color& specular, int shininess) {
	ka = ambient;
	kd = diffuse;
	ks = specular;
	n = shininess;
}
Material::Material() {
}

