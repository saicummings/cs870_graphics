#include "Sphere.h"

Sphere::Sphere(Point4& center, float radius, Material& color)
    : Object(color) {
	c = center;
	r = radius;
}

bool Sphere::intersects(Ray4& ray, Hit& hit) {

	Point4 S = ray.start;
	Vector4 V = ray.direction;

	float as = V * V;
	float bs = 2 * V * (S - c);
	float cs = (S - c) * (S - c) - r * r;

	float ds = bs * bs - 4 * as*cs;
	float t_sphere = -1;
	Point4 P_sphere;
	Vector4 N_sphere;

	

	if (ds > 0) { // Two real roots

		float t1 = (-bs - sqrt(ds)) / (2 * as);
		float t2 = (-bs + sqrt(ds)) / (2 * as);
		
		if (t1 > 0) {
			t_sphere = t1;
		}
		else if (t2 > 0) {
			t_sphere = t2; 
		}
		else {// no intersection
			cout << "Intersect fasle" << endl;
			return false;
		}
		P_sphere = S + t_sphere * V;
		N_sphere = (P_sphere - c).normalized();
		hit = Hit(P_sphere, N_sphere, color, t_sphere);
		//cout << "Intersect true" << endl;
	}

    return true;
}

