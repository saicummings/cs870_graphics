#include "Color.h"


Color::Color(float r, float g, float b): Float4(r, g, b, 1)	{
	red = r;
	green = g;
	blue = b;
}

Color::Color() {
	Color(0.0, 0.0, 0.0);
}

Color Color::operator+(const Color& other) {
	return Color(red + other.red, green + other.green, blue + other.blue);
}

void Color::set(float r, float g, float b) {
	red = r;
	green = g;
	blue = b;
}

float Color::R() {
    return red;
}

float Color::G() {
    return green;
}

float Color::B() {
    return blue;
}
