#include "Hit.h"

Hit::Hit() {
	t_value = 100000.0;
}

float Hit::getTValue() {
	return t_value;
}

Hit::Hit(Point4 p, Vector4 v, Material& m, float t) {
	hit_point = p;
	normal = v;
	material = m;
	t_value = t;
}