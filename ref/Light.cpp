#include "Light.h"

Light::Light(Color c, Point4 p) {
	color = c;
	position = p;
	ambient_color = Color(0.3 * c.R(), 0.3 * c.G(), 0.3 * c.B());
}

Light::Light() {
}
